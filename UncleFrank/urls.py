"""UncleFrank URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.http import HttpResponse


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^apis/', include('apis.urls')),
    url(r'^sadmin/', include('superadmin.urls')),
    url(r'^$', 'base.views.home', name="home"),
    url(r'^address/', include('address.urls')),
    # url(r'^$', 'customer.views.home'),
    # (r'^djga/', include('google_analytics.urls')),

    url(r'^customer/', include("customer.urls")),
    url(r'^page/', include("base.urls")),
    url(r'^service/', include("service.urls")),
    # url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    # url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
]