from service.models import Payment as Pay, DeliveryService, PickupService, ActivityLog
from base import constant
from coupon.models import Coupon, CouponRedemptions
import stripe
from datetime import date
from customer.models import CreditCard

def get_admin_charge_for_courier():
	return

class Payment:
	def __init__(self, request):
		self.request = request

	def get_coipon_discount_pickup(self, coupon, user, cost):
		from coupon.models import PickupCoupon
		from coupon.models import CustomerCoupon
		discount = 0
		today = date.today()
		try:
			customer_coupon = PickupCoupon.objects.get(code=coupon)
			count = CustomerCoupon.objects.filter(coupon=customer_coupon, user=user).count()
			if count <= customer_coupon.number_of_use and customer_coupon.start_date <= today and customer_coupon.end_date >= today:
				if customer_coupon.discount_type == 'percentage':
					discount = round(((cost/100) * float(customer_coupon.discount_value)), 2)
				else:
					discount = float(customer_coupon.discount_value)
				used = CustomerCoupon()
				used.user = user
				used.number_of_use = 1
				used.code = coupon
				used.coupon = customer_coupon
				used.save()
		except:
			pass
		return discount

	def if_coupone_exist(self, coupon):
		"""
			Find if the coupone ilegible for use
		"""
		# Check for expairy date
		from customer.models import Customer
		customer = Customer.objects.get(user=self.request.user)
		today = date.today()
		status = True
		if coupon.start_date <= today and coupon.end_date >= today :
			redemptions = CouponRedemptions.objects.filter(cupon_code=coupon.code)
			redumption_by_consumer = redemptions.filter(used_by=customer)
			no_of_uses = redemptions.count()
			no_of_use_by_consumer = redumption_by_consumer.count()
			if coupon.code_type == 'onetime' :
				if no_of_uses > 0:
					status = False
			else:
				if no_of_uses >= coupon.number_of_use and no_of_use_by_consumer >= number_of_use_per_user:
					#Customer quata
					status = False
		else:
			status = False
		return status

	def is_coupne_available_for_delivery_window(self, coupone_code, delivery_window):
		"""
			check if the coupone code is avaiable for this delivery window.
		"""
		status = True
		try:
			coupon = Coupon.objects.get(code= coupone_code)
			if coupon.delivery_window is not None :
				if coupon.delivery_window.window_name != delivery_window :
					status = False
		except Exception, e:
			status = False
		return status

	def write_coupon_redemption(self, cupon_code, customer, job_id, job_type, value):
		"""
			Write log on coupone redemption table about the coupone code uses.
		"""
		try:
			redemtion = CouponRedemptions()
			redemtion.cupon_code = cupon_code
			redemtion.used_by = customer
			redemtion.job_id = job_id
			redemtion.job_type = job_type
			redemtion.value = value
			redemtion.save()
			return True
		except:
			return False

	
	def promo_code_discount(self, promo, amount):
		"""
			calculate the discount amount and return the amount after discount
		"""
		return_value = amount
		try:
			coupon = Coupon.objects.get(code= promo)
			if self.if_coupone_exist(coupon):
				#calculation goes here
				if coupon.discount_type == 'amount':
					return_value = amount - coupon.discount_value
				else:
					return_value = amount - amount * (coupon.discount_value / 100 )
		except:	
			pass
		return return_value

	def write_log_for_courier_payment(self, courier_job, message):
		try:
			log = ActivityLog()
			log.log_message = message
			log.delevery_service = courier_job
			log.log_type = "The payment is done."
			log.delevery_status = "paid"
			log.logger_name = courier_job.customer.name
			log.save()
			return True
		except:
			return False

	def write_log_for_pickup_payment(self, pickup_job, message):
		try:
			log = ActivityLog()
			log.log_message = message
			log.pickup_service = pickup_job
			log.log_type = "The payment is done."
			log.delevery_status = "paid"
			log.logger_name = pickup_job.customer.name
			log.save()
			return True
		except:
			return False

	def write_log_for_batch_payment(self, courier_jobs, message):
		try:
			for courier_job in courier_jobs:
				try:
					log = ActivityLog()
					log.log_message = message
					log.delevery_service = courier_job
					log.log_type = "The payment is done."
					log.delevery_status = "paid"
					log.save()
				except:
					pass
			return True
		except:
			return False
		

	def write_payment_date_courier(self, courier_job, total_amount):
		try:
			# print admin_fee
			# print ((total_amount * 10) / 100)
			payment = Pay()
			payment.customer = courier_job.customer
			payment.delevery_service = courier_job
			payment.total_cost = total_amount
			payment.paid_amount = total_amount
			payment.admin_fee = ((total_amount * 10) / 100)
			payment.payment_status = "UNPAID"
			payment.save()
			return True
		except Exception, e:
			print e
			return False

	def write_payment_date_courier_coupon(self, courier_job, total_amount, discount, promo_code):
		try:
			# print ((total_amount - discount) * 10 / 100)
			payment = Pay()
			payment.customer = courier_job.customer
			payment.delevery_service = courier_job
			payment.total_cost = total_amount
			payment.paid_amount =   total_amount - discount
			payment.coupon_amount = discount
			payment.coupon_code = promo_code
			payment.admin_fee = ((total_amount - discount)*10/100)
			payment.payment_status = "UNPAID"
			payment.save()
			return True
		except Exception, e:
			print e
			return False

	def write_payment_data_pickup(self, pickup_job, total_cost, paid_amount, online_fee, toll_fee, admin_fee):
		try:
			payment = Pay()
			payment.customer = pickup_job.customer
			payment.pickup_service = pickup_job
			payment.total_cost = total_cost
			payment.paid_amount = paid_amount
			payment.online_fee = online_fee
			payment.toll_fee = toll_fee
			payment.admin_fee = admin_fee
			payment.payment_status = "UNPAID"
			payment.save()
			return True
		except Exception, e:
			print e
			return False

	def pay_for_courier_with_card(self, courier_job, total_cost, card_no, cvv, exp_month, exp_year):
		try:
			stripe.api_key = constant.STRIPE_SECRECT_KEY
			token = stripe.Token.create(card={
				"number": card_no,
				"exp_month": exp_month,
				"exp_year": exp_year,
				"cvc": cvv}, )
			auth_capture = stripe.Charge.create(
				amount=int(total_cost * 100),
				currency="sgd",
				source=token,  # obtained with Stripe.js
				description="Charge for " + courier_job.customer.email,
				capture=False,
				)
			# Splite Payment
			log_message = "Payment completed for courrier job"
			courier_job.auth_capture_token = auth_capture.id
			courier_job.save()
			self.write_log_for_courier_payment(courier_job, log_message)
			self.write_payment_date_courier(courier_job, total_cost)
			return True
		except:
			return False
	
	def pay_for_courier_for_saved_caed(self, card, total_cost, courier_job):
		stripe.api_key = constant.STRIPE_SECRECT_KEY
		try:
			auth_capture = stripe.Charge.create(
				amount=int(total_cost * 100),
				currency="sgd",
				customer=card.strite_id,
				capture=False,
				)
			log_message = "Payment completed for courrier job"
			courier_job.auth_capture_token = auth_capture.id
			courier_job.save()
			self.write_log_for_courier_payment(courier_job, log_message)
			self.write_payment_date_courier(courier_job, total_cost)
			return True
		except Exception, e:
			return False
	
	def pay_for_pickup(self, pickup_job, card, price):
		try:
			stripe.api_key = constant.STRIPE_SECRECT_KEY
			total = int(price['total'] * 100)
			# try:
			stripe.Charge.create(
				amount=total,
				currency="sgd",
				customer=cards[0].strite_id)
			log_message = "Your payment has been completed"
			self.write_log_for_pickup_payment(pickup_job, log_message)
			self.write_payment_data_pickup(pickup_job, price['total'], price['total'], price['online_fee'], price['toll_fee'], price['admine_fee'])
			return True
		except:
			return False

	def pay_for_courier_with_card_promo(self, courier_job, total_cost, promo_code, card_no, cvv, exp_month, exp_year):
		"""
			Pay with new card. Promo code discount will be added
		"""
		pay_amount = self.promo_code_discount(promo_code, total_cost)
		discount = total_cost - pay_amount
		if pay_amount > 0:
			try:
				stripe.api_key = constant.STRIPE_SECRECT_KEY
				token = stripe.Token.create(card={
					"number": card_no,
					"exp_month": exp_month,
					"exp_year": exp_year,
					"cvc": cvv}, )
				auth_capture = stripe.Charge.create(
					amount=int(pay_amount * 100),
					currency="sgd",
					source=token,  # obtained with Stripe.js
					description="Charge for " + courier_job.customer.email,
					capture=False,
					)
				# Splite Payment
				log_message = "Payment completed for courrier job"
				courier_job.auth_capture_token = auth_capture.id
				courier_job.save()
				self.write_log_for_courier_payment(courier_job, log_message)
				self.write_payment_date_courier_coupon(courier_job, total_cost, discount, promo_code)
				self.write_coupon_redemption(promo_code, courier_job.customer, courier_job.id, 'courier', discount)
				return True
			except:
				return False

	def pay_for_courier_for_saved_card_promo(self, card, promo, total_cost, courier_job):
		"""
			Pay with already saved card. Promo code discount will added.
		"""
		pay_amount = self.promo_code_discount(promo, total_cost)
		discount_amount = total_cost - pay_amount
		stripe.api_key = constant.STRIPE_SECRECT_KEY
		try:
			auth_capture = stripe.Charge.create(
				amount=int(pay_amount * 100),
				currency="sgd",
				customer=card.strite_id,
				capture=False,
				)
			log_message = "Payment completed for courier job. "
			courier_job.auth_capture_token = auth_capture.id
			courier_job.save()
			self.write_log_for_courier_payment(courier_job, log_message)
			self.write_payment_date_courier_coupon(courier_job, total_cost, discount_amount, promo)
			self.write_coupon_redemption(promo, courier_job.customer, courier_job.id, 'courier', discount_amount)
			return True
		except Exception, e:
			return False

	def pay_for_batch(self):
		pass

	def get_avaiable_cards(self, customer):
		cards = CreditCard.objects.filter(custmer=customer)
		return cards

	
	def pay_for_pickup_cancellation_fees_with_saved_card(self, pickup_job, card, price):
		try:
			stripe.api_key = constant.STRIPE_SECRECT_KEY
			total = int(price * 100)
			stripe.Charge.create(
				amount=total,
				currency="sgd",
				customer=card.strite_id)
			log_message = "Your Cancellation fees has been paid"
			self.write_log_for_pickup_payment(pickup_job, log_message)
			self.write_payment_data_pickup(pickup_job, price, price, 0, 0, 0)
			return True
		except:
			return False

	def pay_for_pickup_cancellation_fees_with_card(self, pickup_job, total_cost, card_no, cvv, exp_month, exp_year):
		try:
			stripe.api_key = constant.STRIPE_SECRECT_KEY
			token = stripe.Token.create(card={
				"number": card_no,
				"exp_month": exp_month,
				"exp_year": exp_year,
				"cvc": cvv}, )
			stripe.Charge.create(
				amount=int(total_cost * 100),
				currency="sgd",
				source=token,  # obtained with Stripe.js
				description="Charge for " + pickup_job.customer.email
				)
			# Splite Payment
			log_message = "Your Cancellation fees has charged."
			self.write_log_for_pickup_payment(pickup_job, log_message)
			self.write_payment_data_pickup(pickup_job, total_cost, total_cost, 0, 0, 0)
			return True
		except:
			return False

	def pay_with_credite_terms(self, job, cost, express_charge, sercharge, discount, total, admin_fee=0):
		from service.models import CraditeTermPayment
		from customer.models import CreditTerm
		import decimal
		#write log 
		try:
			cradite = CreditTerm.objects.get(customer_id=job.customer.id)
			cradite.credit_term = cradite.credit_term - decimal.Decimal(total)
			cradite.save()
			log_message = "Credit terms"
			self.write_log_for_courier_payment(job, log_message)
			payment = CraditeTermPayment()
			payment.delevery_service = job
			payment.cost = cost
			payment.express_charge = express_charge
			payment.sercharge = sercharge
			payment.discount = discount
			payment.total = total
			payment.admin_fee = admin_fee
			payment.save()
			return True
		except:
			return False

	def if_cradite_term_exists(self, customer):
		from customer.models import CreditTerm
		try:
			cradite = CreditTerm.objects.get(customer_id=customer.id)
			if cradite.credit_term >= 0:
				return True
			else:
				return False
		except:
			return False

	def charge_authorised_capture(self, capture_id):
		try:
			stripe.api_key = constant.STRIPE_SECRECT_KEY
			charge = stripe.Charge.retrieve(capture_id)
			charge.capture()
			return True
		except:
			return False


	# def get_cradite_amount(self, customer):
	# 	from customer.models import CreditTerm
	# 	from service.models import CraditeTermPayment
	# 	payments = CraditeTermPayment.objects.filter(customer=customer)
	# 	total = 0
	# 	for payment in payments:
	# 		total = total + payment.cost
	# 	if 



