from base import constant
import requests
import json

class Notificatoin(object):
    def __init__(self):
        self.fcm_url = "https://fcm.googleapis.com/fcm/send"


    def create_message(self, device_list , title, body, category, job_id):
        noti = dict()
        noti['content_available'] = True
        noti['notification'] = {}
        noti['notification']['body'] = title
        noti['notification']['title'] = body
        noti['notification']['category'] = category
        if job_id != 0:
            noti['notification']['job_id'] = job_id
        noti['data'] = {}
        noti['data']['message'] = body
        noti['data']['title'] = title
        noti['notification']['sound'] = 'default'
        noti['registration_ids'] = device_list
        noti['priority'] = 'high'
        return noti


    def send_notification(self, device_list, title, body, category=0, job_id=0):
        data = self.create_message(device_list=device_list, title=title, body=body, category=category, job_id=job_id)
        header = self.get_header()
        return requests.request("POST", self.fcm_url, data=json.dumps(data), headers=header)


class CustomerNotification(Notificatoin):
    def __init__(self):
        self.fcm_key = constant.GCM_API_KEY_CUSTOMER
        super(CustomerNotification, self).__init__()

    def get_header(self):
        headers = {
            'authorization': "key=" + self.fcm_key,
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        return headers


class DriverNotification(Notificatoin):
    def __init__(self):
        self.fcm_key = constant.GCM_API_KEY_DRIVER
        super(DriverNotification, self).__init__()

    def get_header(self):
        headers = {
            'authorization': "key=" + self.fcm_key,
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        return headers