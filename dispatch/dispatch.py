import requests
from driver.models import Driver
from base import constant
from geo.models import DriverLocation, CustomerLocation
from django.contrib.gis import geos
from django.contrib.gis import measure
from service.models import DeliveryService, CorporateDriverCustomer, Package, ActivityLog
from datetime import datetime
from cart.cart import Cart
from service.models import PickupService, VehicleType
import googlemaps
import json
import time
from base.custome_decorator import postpone
from django.contrib.gis.geos import Point
from service.models import PackageSize
from django.db.models import Q

# from superadmin.views import get_site_configeartions


class Dispatch:
    """
		This class is responsible to dispatch 
		courier and pick up job to the appropriate 
		drivers
	"""
    def __init__(self, req):
        if req is not None:
            self.request = req

	
	def send_push_message_for_pickeup_job_post(self, device_token_list, message, title, category = 0, job_id= 0):
		url = "https://fcm.googleapis.com/fcm/send"
		noti = {}
		noti['content_available'] = True
		noti['notification'] = {}
		noti['notification']['body'] = message
		noti['notification']['title'] = title
		noti['notification']['category'] = category
		noti['notification']['job_id'] = job_id
		noti['data'] = {}
		noti['data']['message'] = message
		noti['data']['title'] = title
		noti['notification']['sound'] = 'default'
		noti['registration_ids'] = device_token_list
		noti['priority'] = 'high'
		headers = {
			'authorization': "key="+constant.GCM_API_KEY_DRIVER,
			'content-type': "application/json",
			'cache-control': "no-cache",
			}
		return  requests.request("POST", url, data=json.dumps(noti), headers=headers)

	def dispatch_pickup_job(self):
		pass


    def __init__(self, request):
        self.request = request

    def find_nearest_active_drivers(self, car_type, origin_location):
        pass

    # def send_push_message_for_pickeup_job_post(self, device_token_list, message, title, category=0, job_id=0):
    #     url = "https://fcm.googleapis.com/fcm/send"
    #     noti = {}
    #     noti['content_available'] = True
    #     noti['notification'] = {}
    #     noti['notification']['body'] = message
    #     noti['notification']['title'] = title
    #     noti['notification']['category'] = category
    #     noti['notification']['job_id'] = job_id
    #     noti['data'] = {}
    #     noti['data']['message'] = message
    #     noti['data']['title'] = title
    #     noti['notification']['sound'] = 'default'
    #     noti['registration_ids'] = device_token_list
    #     noti['priority'] = 'high'
    #     headers = {
    #         'authorization': "key="+constant.GCM_API_KEY_DRIVER,
    #         'content-type': "application/json",
    #         'cache-control': "no-cache",
    #     }
    #     return requests.request("POST", url, data=json.dumps(noti), headers=headers)

    def dispatch_pickup_job(self):
        pass

    def dispatch_courier_job_corporate_with_driver(self, item):
        from .location import Location
        location = Location()
        collection_postal_code = location.get_postal_code(self.request.session['collection-address'])
        delivery_postal_code = location.get_postal_code(self.request.session['delivery-contact-number'])

        corporate = CorporateDriverCustomer.objects.filter(customer=self.request.customer)
        delevery_service = DeliveryService()
        delevery_service.customer = self.request.customer
        # delevery_service.
        delevery_service.collection_name = self.request.session['collection-name']

        delevery_service.collection_building = self.request.session['collection_bulding_name']
        delevery_service.delivery_building = self.request.session['delivery_bulding_name']

        delevery_service.collection_address = self.request.session['collection-address']
        delevery_service.collection_contact_no = self.request.session['collection-contact-number']
        delevery_service.collection_remark = self.request.session['collection-remarks']
        delevery_service.delivery_name = self.request.session['delivery-name']
        delevery_service.delivery_address = self.request.session['delivery-address']
        delevery_service.delivery_contact_no = self.request.session['delivery-contact-number']
        delevery_service.delevery_remarks = self.request.session['delivery-remarks']
        delevery_service.collection_date = datetime.strptime(self.request.session['cellection_date'], '%d %b %Y').date()
        delevery_service.collection_time = self.request.session['cellection-time']
        delevery_service.delevery_date = datetime.strptime(self.request.session['delivery_date'], '%d %b %Y').date()
        delevery_service.delevery_time = self.request.session['delivery-time']
        delevery_service.collection_unit_number = self.request.session['collection-unit-number']
        # delevery_service.collection_postal_code = self.request.session['collection-postal-code']
        delevery_service.delivery_unit_number = self.request.session['delivery-unit-number']
        # delevery_service.delivery_postal_code = self.request.session['delivery-postal-code']
        delevery_service.distance = self.request.session['distance']
        delevery_service.eta = self.request.session['eta']
        delevery_service.rated = 'not_rated'
        delevery_service.delevery_status = 'assigned'
        delevery_service.taken_by = corporate[0].driver
        delevery_service.total_cost = self.request.session['total_price']
        delevery_service.payment_status = False
        delevery_service.status = False
        delevery_service.is_corporate = True
        delevery_service.auto_assigned = True
        delevery_service.collection_postal_code = collection_postal_code
        delevery_service.delivery_postal_code = delivery_postal_code
        delevery_service.save()
        cart = Cart(self.request)
        items = item
        promo_code = self.request.session['promo_code']

        for item in items:
            package = Package()
            package.service = delevery_service
            package.quantity = item.quantity
            package.image = item.image
            package.weight = item.weight
            package.length = item.length
            package.width = item.width
            package.height = item.height
            package.size = item.size
            package.unit_price = item.unit_price
            package.save()
        cart.clear_items()
        log = ActivityLog()
        log.delevery_service = delevery_service
        log.delevery_status = "assigned"
        log.log_message = constant.ASSIGNED_JOB
        log.logger_name = "ADMIN"
        log.log_type = constant.ASSIGNED_JOB
        log.save()
        return delevery_service.id

    def dispatch_courier_job_corporate_without_driver(self, item):
        from .location import Location
        location = Location()
        collection_postal_code = location.get_postal_code(self.request.session['collection-address'])
        delivery_postal_code = location.get_postal_code(self.request.session['delivery-contact-number'])

        delevery_service = DeliveryService()
        delevery_service.customer = self.request.customer
        # delevery_service.
        delevery_service.collection_name = self.request.session['collection-name']

        delevery_service.collection_building = self.request.session['collection_bulding_name']
        delevery_service.delivery_building = self.request.session['delivery_bulding_name']

        delevery_service.collection_address = self.request.session['collection-address']
        delevery_service.collection_contact_no = self.request.session['collection-contact-number']
        delevery_service.collection_remark = self.request.session['collection-remarks']
        delevery_service.delivery_name = self.request.session['delivery-name']
        delevery_service.delivery_address = self.request.session['delivery-address']
        delevery_service.delivery_contact_no = self.request.session['delivery-contact-number']
        delevery_service.delevery_remarks = self.request.session['delivery-remarks']
        delevery_service.collection_date = datetime.strptime(self.request.session['cellection_date'], '%d %b %Y').date()
        delevery_service.collection_time = self.request.session['cellection-time']
        delevery_service.delevery_date = datetime.strptime(self.request.session['delivery_date'], '%d %b %Y').date()
        delevery_service.delevery_time = self.request.session['delivery-time']
        delevery_service.collection_unit_number = self.request.session['collection-unit-number']
        # delevery_service.collection_postal_code = self.request.session['collection-postal-code']
        delevery_service.delivery_unit_number = self.request.session['delivery-unit-number']
        # delevery_service.delivery_postal_code = self.request.session['delivery-postal-code']
        delevery_service.distance = self.request.session['distance']
        delevery_service.eta = self.request.session['eta']
        delevery_service.rated = 'not_rated'
        delevery_service.delevery_status = 'pending'
        delevery_service.total_cost = self.request.session['total_price']
        delevery_service.payment_status = False
        delevery_service.status = False
        delevery_service.is_corporate = True
        delevery_service.collection_postal_code = collection_postal_code
        delevery_service.delivery_postal_code = delivery_postal_code
        delevery_service.save()
        cart = Cart(self.request)
        items = item

        for item in items:
            package = Package()
            package.service = delevery_service
            package.quantity = item.quantity
            package.image = item.image
            package.weight = item.weight
            package.length = item.length
            package.width = item.width
            package.height = item.height
            package.size = item.size
            package.unit_price = item.unit_price
            package.save()
        cart.clear_items()
        return delevery_service.id

    def dispatch_courier_job_normal(self, item):
        from .location import Location
        location = Location()
        collection_postal_code = location.get_postal_code(self.request.session['collection-address'])
        delivery_postal_code = location.get_postal_code(self.request.session['delivery-contact-number'])

        delevery_service = DeliveryService()
        delevery_service.customer = self.request.customer
        # delevery_service.
        delevery_service.collection_name = self.request.session['collection-name']
        
        delevery_service.collection_building = self.request.session['collection_bulding_name']
        delevery_service.delivery_building = self.request.session['delivery_bulding_name']
        
        delevery_service.collection_address = self.request.session['collection-address']
        delevery_service.collection_contact_no = self.request.session['collection-contact-number']
        delevery_service.collection_remark = self.request.session['collection-remarks']
        delevery_service.delivery_name = self.request.session['delivery-name']
        delevery_service.delivery_address = self.request.session['delivery-address']
        delevery_service.delivery_contact_no = self.request.session['delivery-contact-number']
        delevery_service.delevery_remarks = self.request.session['delivery-remarks']
        delevery_service.collection_date = datetime.strptime(self.request.session['cellection_date'], '%d %b %Y').date()
        delevery_service.collection_time = self.request.session['cellection-time']
        delevery_service.delevery_date = datetime.strptime(self.request.session['delivery_date'], '%d %b %Y').date()
        delevery_service.delevery_time = self.request.session['delivery-time']
        delevery_service.collection_unit_number = self.request.session['collection-unit-number']
        # delevery_service.collection_postal_code = self.request.session['collection-postal-code']
        delevery_service.delivery_unit_number = self.request.session['delivery-unit-number']
        # delevery_service.delivery_postal_code = self.request.session['delivery-postal-code']
        delevery_service.distance = self.request.session['distance']
        delevery_service.eta = self.request.session['eta']
        delevery_service.rated = 'not_rated'
        delevery_service.delevery_status = 'pending'
        delevery_service.total_cost = self.request.session['total_price']
        delevery_service.payment_status = False
        delevery_service.status = False
        delevery_service.collection_postal_code = collection_postal_code
        delevery_service.delivery_postal_code = delivery_postal_code
        delevery_service.save()
        cart = Cart(self.request)
        items = item
        promo_code = self.request.session['promo_code']

        for item in items:
            package = Package()
            package.service = delevery_service
            package.quantity = item.quantity
            package.image = item.image
            package.weight = item.weight
            package.length = item.length
            package.width = item.width
            package.height = item.height
            package.size = item.size
            package.unit_price = item.unit_price
            package.save()
        cart.clear_items()
        return delevery_service.id

    def find_find_assigned_driver(self, customer):
        """
			If corporate client then find addigned drivers
			if normal client then find favorate drivers
		"""
        pass

    def if_driver_active_to_assign_courier_job(self):
        """
			Find is a driver is active and not engaged
			to take a courier job
		"""
        pass

    def dispatch_courrier_order(self, courier_job):
        """
			If the customer is and corporate client
			then chek if there is any driver is assigned for him.
			if there is assigned driver and driver is free to take
			the job then assign the job to him. if the driver is not 
			free or there is no assighed hob for te driver then 
			send the job to the super admin and let super admin to assign
			the job to driver
		"""
        pass

    def print_background(self):
        print("test")

    def get_all_drivers_location(self):
        """
		Get All drivers location
		"""
        drivers = DriverLocation.objects.all()
        return drivers

    def get_all_customer_location(self):
        """
        Get All drivers location
        """
        customers = CustomerLocation.objects.all()
        return customers

    def get_nearest_active_driver(self, lat, lng, distance):
        """
			get the nearest driver id (nearest to ferthers sequence)
		"""
        current_point = Point(float(lng), float(lat))
        distance_from_point = {'km': distance}
        drivers = DriverLocation.gis.filter(location__distance_lte=(current_point, measure.D(**distance_from_point)))
        drivers = drivers.distance(current_point).order_by('distance')
        return drivers

    def get_driver_location_id(self, driver_id):
        driver = None
        try:
            driver = DriverLocation.objects.get(driver_id=driver_id)
        except:
            pass
        return driver

    
    # def get_extimate_price(self, type_id, from_address, to_address):
    #     from dispatch.location import Location
    #     datype = "Weekday"
    #     today = datetime.datetime.today()
    #     if today.weekday() == 6:
    #         datype = "Weekend"
    #     elif HolidayList.objects.filter(date=today).count() > 0:
    #         datype = "Weekend"
    #     price = PickUpRate.objects.filter(vehicle_type=VehicleType.objects.get(id=type_id), day_type=datype)
    #     location = Location()
    #     deisance = location.get_diatance(from_address, to_address)
    #     if deisance:
    #         fare = price[0].base_fare + (price[0].rate*deisance['distance']/1000)  + (price[0].waiting_charge *deisance['time']/60)
    #     else:
    #         fare = 0
    #     return round(fare, 2)
    def create_pick_job(self, pickupAddress, dropOffAddess, noteForDriver, vehicleType, customer, coupon_code):
        """
			Create a Pickup order
		"""
        from .location import Location
        location = Location()
        pickup_postal_code = location.get_postal_code(pickupAddress)
        dropoff_postal_code = location.get_postal_code(dropOffAddess)

        pickup = PickupService()
        gmaps = googlemaps.Client(key=constant.GOOGLE_MAP_KEY)
        pick_up_geo = gmaps.geocode(address=pickupAddress + "")
        drop_off_geo = gmaps.geocode(address=dropOffAddess + "")


        pickup_address = pick_up_geo[0]['formatted_address']
        dropoff_address = drop_off_geo[0]['formatted_address']
        pickup.pickup_address = pickupAddress
        pickup.pickup_address_lat = pick_up_geo[0]['geometry']['location']['lat']
        pickup.pickup_address_lng = pick_up_geo[0]['geometry']['location']['lng']
        pickup.dropoff_address = dropOffAddess
        pickup.dropoff_address_lat = drop_off_geo[0]['geometry']['location']['lat']
        pickup.dropoff_address_lng = drop_off_geo[0]['geometry']['location']['lng']
        # eta distance
        directions_results = gmaps.distance_matrix(
            pickupAddress + "",
            dropOffAddess + "",
            mode=None,
            language=None,
            avoid=None,
            units="metric",
            departure_time=None,
            arrival_time=None,
            transit_mode=None,
            transit_routing_preference=None,
            traffic_model=None)
        approx_distance = str(directions_results['rows'][0]['elements'][0]['distance']['text'])
        pickup.distance = approx_distance
        pickup.remarks = noteForDriver
        pickup.vehicle_type = VehicleType.objects.get(id=vehicleType)
        pickup.customer = customer
        pickup.rated = 'not_rated'
        pickup.pickup_status = 'pending'
        pickup.coupone = coupon_code
        pickup.pickup_postal_code = pickup_postal_code
        pickup.dropoff_postal_code = dropoff_postal_code
        #get extimated cost
        # pickup.est_cost = self.get_extimate_price(vehicleType, pickupAddress, dropOffAddess)
        pickup.save()
        return pickup


	# @postpone
	# @celery_app.task()
	def dispatch_job_to_nearest_driver(self, pickup_job):
		"""
			Request drivers for accepting the job one by one
			from nearest to ferthest
		"""
        # site_config = superadmin.views.get_site_configeartions()
        puck_up_lat = pickup_job.pickup_address_lat
        puck_up_lng = pickup_job.pickup_address_lng
        if site_config:
        	distance = site_config.driver_radius
        	waiting_time = site_config.waiting_time_for_drivers
        else:
            distance = 5
            waiting_time = 15
        near_nearest_driver_list = self.get_nearest_active_driver(puck_up_lat, puck_up_lng, distance)
        token_list = []

        for driver in near_nearest_driver_list:
            try:
                d = Driver.objects.get(id=driver.driver_id)
                token_list.append(d.device_token)
            except Exception, e:
                continue
        self.send_push_message_for_pickeup_job_post(token_list, "New Job", "New Pickup Job Posted", 2, pickup_job.id)
        return True


    def confirm_customer_fo_pickup_taken(self, customer_token, message, title, driver_id, job_id=0, eta=0):
        """
            For pickup category is 2 and for courier category is 1
        """
        url = "https://fcm.googleapis.com/fcm/send"
        noti = {}
        noti['content_available'] = True
        noti['notification'] = {}
        noti['notification']['body'] = message
        noti['notification']['title'] = title
        noti['notification']['category'] = 2
        noti['notification']['driver_id'] = driver_id
        noti['notification']['job_id'] = job_id
        noti['notification']['eta'] = eta
        noti['data'] = {}
        noti['data']['message'] = message
        noti['data']['title'] = title
        noti['data']['category'] = 2
        noti['notification']['sound'] = 'default'
        noti['registration_ids'] = [customer_token]
        noti['priority'] = 'high'
        headers = {
            'authorization': "key="+constant.GCM_API_KEY_CUSTOMER,
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        return requests.request("POST", url, data=json.dumps(noti), headers=headers)

    def write_activity_log_pickup_cancelled(self, pickup_job, reason):
        log = ActivityLog()
        log.pickup_service = pickup_job
        log.pickup_status = "cancelled"
        log.log_message = reason
        log.logger_name = pickup_job.customer.name
        log.log_type = constant.CANCELLED_JOB
        log.save()
        return True

    def cancell_job_by_customer(self, pickup_job, reason, comment):
        from django.utils import timezone
        self.write_activity_log_pickup_cancelled(pickup_job, reason)
        pickup_job.pickup_status = "cancelled"
        pickup_job.job_cancelled_time = timezone.now()
        pickup_job.cancel_reason = reason
        pickup_job.cancell_note = comment
        pickup_job.save()
        from payment.pricecalculation import PriceCalculation
        from payment.payment import Payment
        price = PriceCalculation(self.request)
        cancellation_fee = price.get_cancelation_fees_for_pickup(pickup_job)
        if cancellation_fee > 0 :
            ##charge cancellation
            pay = Payment(self.request)
            cards = pay.get_avaiable_cards(pickup_job.customer)
            for card in cards:
                if pay.pay_for_pickup_cancellation_fees_with_saved_card(pickup_job, card, cancellation_fee):
                    break
        return pickup_job
        # need to add functional for charage cancellation fees

    def confirm_customer_fo_pickup_completed(self, customer_token, message, title):
        """
            For pickup category is 2 and for courier category is 1
        """
        url = "https://fcm.googleapis.com/fcm/send"
        noti = {}
        noti['content_available'] = True
        noti['notification'] = {}
        noti['notification']['body'] = message
        noti['notification']['title'] = title
        noti['notification']['category'] = 3
        noti['data'] = {}
        noti['data']['message'] = message
        noti['data']['title'] = title
        noti['data']['category'] = 3
        noti['notification']['sound'] = 'default'
        noti['registration_ids'] = [customer_token]
        noti['priority'] = 'high'
        headers = {
            'authorization': "key="+constant.GCM_API_KEY_CUSTOMER,
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        return requests.request("POST", url, data=json.dumps(noti), headers=headers)
