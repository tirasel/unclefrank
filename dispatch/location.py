import googlemaps
from base import constant

class Location(object):
	def __init__(self):
		self.api_key = constant.GOOGLE_MAP_KEY
		self.gmap = googlemaps.Client(key=self.api_key)

	def get_diatance(self, location_a, location_b):
		result = dict()
		# try:
		directions_results = self.gmap.distance_matrix(
			location_a,
			location_b,
			mode=None,
			language=None,
			avoid=None,
			units="metric",
			departure_time=None,
			arrival_time=None,
			transit_mode=None,
			transit_routing_preference=None,
			traffic_model=None
			)
		result['distance'] = int(directions_results['rows'][0]['elements'][0]['distance']['value'])
		result['time'] = int(directions_results['rows'][0]['elements'][0]['duration']['value'])
		# except:
		# 	result = None
		return result

	def get_driver_eta(self, pickup_address, driver_lat, driver_long):
		result = dict()
		# try:
		directions_results = self.gmap.distance_matrix(
			origins=pickup_address,
			destinations=str(driver_lat) + ',' + str(driver_long),
			mode=None,
			language=None,
			avoid=None,
			units="metric",
			departure_time=None,
			arrival_time=None,
			transit_mode=None,
			transit_routing_preference=None,
			traffic_model=None
			)
		result['distance'] = int(directions_results['rows'][0]['elements'][0]['distance']['value'])
		result['time'] = int(directions_results['rows'][0]['elements'][0]['duration']['value'])
		return result

	def get_postal_code(self, address):
		postale_code = ""
		try:
			locations = self.gmap.geocode(address)
			data = locations[0]['address_components']
			for d in data:
				if d['types'][0] == 'postal_code':
					postale_code = d['long_name']
					break
		except:
			pass
		if postale_code != "" :
			return postale_code[:2]
		else:
			return postale_code