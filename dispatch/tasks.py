from UncleFrank import celery_app
import time
from service.models import PickupService
from dispatch import Dispatch
from driver.models import Driver
import requests
import json
from django.utils import timezone
from base import constant
from .notification import CustomerNotification, DriverNotification
from superadmin.models import PushMessage



def cancell_job_by_system(job):
	job.pickup_status = 'cancelled'
	job.cancell_note = "No Driver Availbale"
	job.cancel_reason = "No Driver Availbale"
	job.status = False
	job.job_cancelled_time = timezone.now()
	job.save()

def get_vehicle_type(deiver):
	from service.models import VehicleType
	from driver.models import Vechile
	vechile = Vechile.objects.get(driver=deiver)
	return VehicleType.objects.get(id=vechile.vechile_type)

@celery_app.task()
def notify_driver_for_courier_job(job_id):
	from django.db.models import Q
	driver_notification = DriverNotification()
	drivers = Driver.objects.filter(~Q(driver_service_type="pickup"))
	device_token = []
	for d in drivers:
		if d.device_token:
			device_token.append(d.device_token)
	try:
		message = PushMessage.objects.all()
		title = message[0].new_courier_job
		message = message[0].new_courier_job
	except:
		title = "New Courier Job"
		message = "New courier job posted."
	driver_notification.send_notification(device_token, title, message, 1, job_id)
	return True

@celery_app.task()
def dispatch_job_to_nearest_driver(pickup_job_id):
	"""
			Request drivers for accepting the job one by one
			from nearest to ferthest
		"""
	# site_config = superadmin.views.get_site_configeartions()
	customer_notification = CustomerNotification()
	driver_notification = DriverNotification()
	pickup_job = PickupService.objects.get(id=pickup_job_id)
	puck_up_lat = pickup_job.pickup_address_lat
	puck_up_lng = pickup_job.pickup_address_lng
	dispatch = Dispatch(None)
	# if site_config:
	# 	distance = site_config.driver_radius
	# 	waiting_time = site_config.waiting_time_for_drivers
	# else:
	try:
		from superadmin.models import SiteConfigeration
		data = SiteConfigeration.objects.all().filter('-id')
		distance = data[0].driver_radius
		waiting_time = data[0].waiting_time_for_drivers + 3
	except:
		distance = 5
		waiting_time = 18
	near_nearest_driver_list = dispatch.get_nearest_active_driver( puck_up_lat, puck_up_lng, distance)
	# print("ok")
	# print("Driver No"+str(near_nearest_driver_list.count()))

	#the following code is temporary
	# and will remove shortly
	check_job = PickupService.objects.get(id=pickup_job_id)
	for driver in near_nearest_driver_list:
		try:
			d = Driver.objects.get(id=driver.driver_id)
			if driver.get_is_active() and d.driver_service_type != "courier" and get_vehicle_type(d) == pickup_job.vehicle_type: # also driver vechile type will matched
				token = d.device_token
				title = "New Job"
				message = "New Pickup Job Posted"
				driver_notification.send_notification([token], title, message, 2, pickup_job.id)
			else:
				continue
		except Exception, e:
			print(e)
			continue
		time.sleep(20)
		check_job = PickupService.objects.get(id=pickup_job_id)
		if check_job.pickup_status != "pending":
			break
	if check_job.pickup_status == "pending":
		"""
		IF THE JOB IS STILL NOT TAKEN, JOB WILL BE CANCELLED
		"""
		# inform_customer_no_job_found([check_job.customer.device_token], check_job.id)
		try:
			message = PushMessage.objects.all()
			message_title = message[0].courier_job_cancel_by_system
			message_body = message[0].courier_job_cancel_by_system
		except:
			message_title = "Job cancelled"
			message_body = "No driver found for your job. Your job jas been cancelled."
		customer_notification.send_notification([check_job.customer.device_token], message_title, message_body, 5, check_job.id) 
		cancell_job_by_system(check_job)


	return True