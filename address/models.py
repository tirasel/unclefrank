from django.db import models

# Create your models here.

class Address(models.Model):
	post_code = models.IntegerField(default=0)
	bldg_no = models.CharField(max_length=7, null=True, default=None)
	street_name = models.CharField(max_length=32, null=True, default=None)
	bldg_name = models.CharField(max_length=45, null=True, default=None)
	floor = models.CharField(max_length=5, null=True, default=None)
	unit = models.CharField(max_length=5, null=True, default=None)

	def __unicode__(self):
		return self.bldg_name

class Postalcode(models.Model):
	post_code = models.IntegerField(default=0)
	address_type = models.CharField(max_length=1, null=True, default=None)
	buliding_no = models.CharField(max_length=7, null=True, default=None)
	street_key = models.CharField(max_length=7, null=True, default=None)
	building_key = models.CharField(max_length=6, null=True, default=None)

	def __unicode__(self):
		return str(self.post_code)

class Street(models.Model):
	street_key = models.CharField(max_length=7, null=True, default=None)
	street_name = models.CharField(max_length=32, null=True, default=None)
	filler = models.CharField(max_length=6, null=True, default=None)