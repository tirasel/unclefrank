from django.shortcuts import render
from django.shortcuts import render, render_to_response, redirect
from django.core.context_processors import csrf
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from .forms import AddressForm
from .models import Address, Postalcode, Street
from django.core import serializers
import csv
from base.custome_decorator import postpone

# Create your views here.

def address_setup(request):
	if not request.user.is_active:
		return redirect('/sadmin/login/')
	form = AddressForm()
	addresses = Address.objects.all()
	if request.method == 'POST':
		form = AddressForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
		return redirect('/address/setup/')
	context = {
   		'form': form,
   		'addresses' : addresses,
	}
	context.update(csrf(request))
	return render(request, 'address/address_setup.html', context)

def address_update(request, address_id):
	address = Address.objects.get(id=address_id)
	address = [address]
	form = AddressForm(instance=address[0])
	if request.method == 'POST':
		form = AddressForm(request.POST, instance=address[0])
		if form.is_valid():
			form.save()
			return redirect('/address/setup/')
	context = {
        'form': form,
        'address': address,
    }
	context.update(csrf(request))
	return render(request, 'address/address_setup.html', context)

def address_delete(request, address_id):
	address = Address.objects.get(id=address_id)
	address.delete(False)
	return redirect('/address/setup/')

def upload_csv(request):
	if request.method == 'POST':
		allAddresses = Address.objects.all().delete()
		csvFile = request.FILES['file']
		reader = csv.DictReader(csvFile)
		for row in reader:
			form = Address()
			form.post_code = row['POSTCODE']
			form.bldg_no = row['BLDGNO']
			form.street_name = row['STREETNAME']
			form.bldg_name = row['BLDGNAME']
			form.floor = row['FLOOR']
			form.unit = row['UNIT']
			form.save()
		return redirect('/address/setup/')

@csrf_exempt
def address_check(request):
	if request.method == 'POST':
		try:
			if request.POST['type'] == "getStreet":
				address = Address.objects.filter(post_code=request.POST['post_code'])
				return JsonResponse(serializers.serialize('json', address), safe=False)
			elif request.POST['type'] == "checkAddress":
				try:
					address = Address.objects.get(post_code=request.POST['post_code'],
						street_name=request.POST['street_name'],
						bldg_name=request.POST['building_name'],
						floor=request.POST['floor'],
						unit=request.POST['unit'])
					return JsonResponse({'result':'This is a valid address'})
				except Address.DoesNotExist:
					return JsonResponse({'result':'Address is not valid'})
		except Exception, e:
			print e
	return render(request, 'address/address_check.html')
@postpone
def add_postal(file):
	lines = file.readlines()
	for line in lines:
		postal_code = line[0:6]
		address_type = line[6:7]
		bulding_number = line[7:14]
		street_key = line[14:21]
		bulding_key = line[21:27]
		data = Postalcode()
		data.post_code = postal_code.strip()
		data.address_type = address_type.strip()
		data.buliding_no = bulding_number.strip()
		data.street_key = street_key.strip()
		data.building_key = bulding_key.strip()
		data.save()

def postcode_txt(request):
	arg = {}
	arg.update(csrf(request))
	data = Postalcode.objects.all()
	arg['data'] = data
	arg['postcode'] = Postalcode.objects.all()
	if request.method == 'POST':
		Postalcode.objects.all().delete()
		add_postal(request.FILES['file'])
		# lines = request.FILES['file'].readlines()
		# for line in lines:
		# 	postal_code = line[0:6]
		# 	address_type = line[6:7]
		# 	bulding_number = line[7:14]
		# 	street_key = line[14:21]
		# 	bulding_key = line[21:27]
		# 	data = Postalcode()
		# 	data.post_code = postal_code.strip()
		# 	data.address_type = address_type.strip()
		# 	data.buliding_no = bulding_number.strip()
		# 	data.street_key = street_key.strip()
		# 	data.building_key = bulding_key.strip()
		# 	data.save()
		arg['message'] = "Successfully updated"
		return redirect('/address/postcode_txt/')
	return render(request, 'address/post_code.html', arg)

def street_txt(request):
	arg = {}
	arg.update(csrf(request))
	# data = Postalcode.objects.all()
	# arg['data'] = data
	arg['street'] = Street.objects.all()
	if request.method == 'POST':
		Street.objects.all().delete()
		lines = request.FILES['file'].readlines()
		for line in lines:
			street_key = line[0:7]
			street_name = line[7:39]
			tag_flag = line[39:40]
			data = Street()
			data.street_key = street_key.strip()
			data.street_name = street_name.strip()
			data.filler = tag_flag.strip()
			data.save()
		arg['message'] = "Successfully updated"
		return redirect('/address/street_txt/')
	return render(request, 'address/street.html', arg)
