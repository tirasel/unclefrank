# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0002_auto_20161005_1512'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='bldg_name',
            field=models.CharField(default=None, max_length=45, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='bldg_no',
            field=models.CharField(default=None, max_length=7, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='floor',
            field=models.CharField(default=None, max_length=5, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='post_code',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='address',
            name='street_name',
            field=models.CharField(default=None, max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='unit',
            field=models.CharField(default=None, max_length=5, null=True),
        ),
    ]
