# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_code', models.CharField(max_length=6)),
                ('bldg_no', models.CharField(max_length=7)),
                ('street_name', models.CharField(max_length=32)),
                ('bldg_name', models.CharField(max_length=45)),
                ('floor', models.CharField(max_length=5)),
                ('unit', models.CharField(max_length=5)),
            ],
        ),
    ]
