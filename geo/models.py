from __future__ import unicode_literals

from django.contrib.gis.db import models as gis_models
from django.contrib.gis import geos
from django.db import models
from driver.models import Driver
from customer.models import Customer
import datetime
from django.utils import timezone

# Create your models here.


class DriverLocation(models.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    driver_id = models.IntegerField()
    location = gis_models.PointField(u"longitude/latitude",
                                     geography=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    gis = gis_models.GeoManager()
    objects = models.Manager()
    def __unicode__(self):
    	return self.driver_id

    def get_lat(self):
    	return self.location.y

    def get_lng(self):
    	return self.location.x

    def get_driver_name(self):
        try:
            driver = Driver.objects.get(id=self.driver_id)
            return driver.name
        except:
            return None
    
    def get_driver_type(self):
        try:
            driver = Driver.objects.get(id=self.driver_id)
            return driver.driver_service_type
        except:
            return None

    def def_driver_vechile_type(self):
        try:
            driver = Driver.objects.get(id=self.driver_id)
            vechile = driver.get_vechiles()
            return vechile.get_vehicle_name()
        except:
            return None

    def get_is_active(self):
        # try:
        now = timezone.now()
        time_diffrence = now - self.updated_at 
        driver = Driver.objects.get(id=self.driver_id)
        if int(time_diffrence.total_seconds()) < 60 and driver:
            return True
        else:
            return False
        # except:
        #     return None

class CustomerLocation(models.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    customer_id = models.IntegerField()
    location = gis_models.PointField(u"longitude/latitude",
                                     geography=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    gis = gis_models.GeoManager()
    objects = models.Manager()
    def __unicode__(self):
        return self.customer_id

    def get_lat(self):
        return self.location.y

    def get_lng(self):
        return self.location.x

    def get_customer_name(self):
        try:
            customer = Customer.objects.get(id=self.customer_id)
            return customer.name
        except:
            return None

    def get_driver_type(self):
        return "consumer"

    def get_is_active(self):
        return True