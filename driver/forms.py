from django import forms
from .models import DriverFiles

class DriverFilesForm(forms.ModelForm):
    class Meta:
    	model = DriverFiles
    	fields = ['driver', 'file_name', 'file']