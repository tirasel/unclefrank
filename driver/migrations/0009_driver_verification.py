# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0008_driver_profile_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='verification',
            field=models.CharField(default=None, max_length=5, null=True),
        ),
    ]
