# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0020_auto_20161128_1330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='driver_category',
            field=models.CharField(default=b'NORMAL', max_length=100, null=True, choices=[(b'NORMAL', b'NORMAL'), (b'BRONZE', b'BRONZE'), (b'SILVER', b'SILVER'), (b'GOLD', b'GOLD')]),
        ),
    ]
