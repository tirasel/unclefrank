# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0010_driver_driver_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='driver_service_type',
            field=models.CharField(default=b'both', max_length=10, null=True, choices=[(b'courier', b'courier'), (b'pickup', b'pickup'), (b'both', b'both')]),
        ),
        migrations.AddField(
            model_name='driver',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
