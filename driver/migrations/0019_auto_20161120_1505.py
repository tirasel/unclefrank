# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0018_auto_20161113_1453'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='device_id',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='driver',
            name='device_token',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
    ]
