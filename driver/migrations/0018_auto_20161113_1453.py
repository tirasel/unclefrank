# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0017_driver_driver_cateogory'),
    ]

    operations = [
        migrations.RenameField(
            model_name='driver',
            old_name='driver_cateogory',
            new_name='driver_category',
        ),
    ]
