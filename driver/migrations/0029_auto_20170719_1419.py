# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0028_driver_ride_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='ride_status',
            field=models.CharField(default=b'available', max_length=225, null=True, choices=[(b'available', b'available'), (b'ontheway', b'ontheway'), (b'arrived', b'arrived'), (b'ontrip', b'ontrip'), (b'completed', b'completed')]),
        ),
    ]
