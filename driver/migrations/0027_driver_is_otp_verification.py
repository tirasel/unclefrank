# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0026_driverfiles'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='is_otp_verification',
            field=models.BooleanField(default=False),
        ),
    ]
