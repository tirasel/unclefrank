# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0015_auto_20161026_2016'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bankaccount',
            name='stripe_id',
        ),
        migrations.RemoveField(
            model_name='bankaccount',
            name='stripe_status',
        ),
        migrations.AddField(
            model_name='driver',
            name='stripe_id',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='driver',
            name='stripe_status',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
    ]
