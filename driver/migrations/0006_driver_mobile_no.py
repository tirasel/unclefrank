# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0005_auto_20160918_1440'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='mobile_no',
            field=models.CharField(default=None, max_length=255, unique=True, null=True),
        ),
    ]
