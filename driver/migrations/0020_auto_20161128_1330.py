# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0019_auto_20161120_1505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='mobile_no',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
