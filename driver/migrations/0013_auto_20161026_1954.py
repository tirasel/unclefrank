# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0012_auto_20161022_1328'),
    ]

    operations = [
        migrations.AddField(
            model_name='bankaccount',
            name='cvv',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='expiry_date',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='external_reff_no',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='last_digits',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='bankaccount',
            name='strite_id',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
    ]
