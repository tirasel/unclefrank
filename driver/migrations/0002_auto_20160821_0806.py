# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='driver',
            name='active',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='address',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='contact_no',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='driving_licence',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='licence_no',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='national_id',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='driver',
            name='vehicle_type',
        ),
        migrations.AddField(
            model_name='driver',
            name='driving_licence_photo',
            field=models.ImageField(default=None, null=True, upload_to=b'driver/licence', blank=True),
        ),
        migrations.AddField(
            model_name='driver',
            name='name',
            field=models.CharField(default=None, max_length=255),
        ),
        migrations.AddField(
            model_name='driver',
            name='nric_photo',
            field=models.ImageField(default=None, null=True, upload_to=b'driver/nrc', blank=True),
        ),
        migrations.AlterField(
            model_name='driver',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
    ]
