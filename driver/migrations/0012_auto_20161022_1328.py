# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0011_auto_20161020_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driver',
            name='is_active',
            field=models.BooleanField(default=False),
        ),
    ]
