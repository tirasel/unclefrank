# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0019_driverstatus'),
        ('driver', '0009_driver_verification'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='driver_type',
            field=models.OneToOneField(null=True, default=None, to='superadmin.DriverStatus'),
        ),
    ]
