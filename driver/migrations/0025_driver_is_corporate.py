# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0024_auto_20170110_1901'),
    ]

    operations = [
        migrations.AddField(
            model_name='driver',
            name='is_corporate',
            field=models.BooleanField(default=False),
        ),
    ]
