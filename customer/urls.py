from django.conf.urls import include,url
from views import CustomerProfileView
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from .import views

urlpatterns = [
    #url(r'^admin/', admin.site.urls),

    url(r'^$', 'customer.views.load_profile'),

    url(r'^accounts/', include('allauth.urls')),


    url(r'^register/$', 'customer.views.register_customer'),
    url(r'^verify/$', 'customer.views.customer_virificarion'),
    url(r'^details/$', 'customer.views.customer_details'),
    url(r'^profile/$', 'customer.views.load_profile'),
    url(r'^home/$', 'customer.views.load_profile'),
    url(r'^payment/$', 'customer.views.payment'),
    url(r'^payment/delete/(?P<payment_id>\d+)$', 'customer.views.payment_delete'),
    url(r'^booking/$', 'customer.views.bookCourier'),
    url(r'^uploadCustImage/$', 'customer.views.uploadCustomerImage'),
    url(r'^location/$', 'customer.views.location'),
    url(r'^change_location/$', 'customer.views.change_location'),
    url(r'^resendcode/$', 'customer.views.resend_sms'),
    url(r'^courier/$', 'customer.views.primary_courier_calc'),
    #pages

    url(r'^about/$', 'customer.views.dashboard_about'),
    url(r'^faq/$', 'customer.views.dashboard_faq'),
    url(r'^feedback/$', 'customer.views.feedback'),
    url(r'^rate/$', 'customer.views.rate'),
    url(r'^rate-pickup/$', 'customer.views.rate_pickup'),
    url(r'^test_email/$', 'customer.views.test_email'),
    url(r'^add_pickup_coupon/$', 'customer.views.add_pickup_coupon'),
]
