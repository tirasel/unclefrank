# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0003_auto_20160731_1100'),
    ]

    operations = [
        migrations.CreateModel(
            name='CreditCard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=225)),
                ('card_no', models.CharField(max_length=225)),
                ('contact_no', models.CharField(max_length=225)),
                ('expiry_date', models.CharField(max_length=225)),
                ('external_reff_no', models.CharField(max_length=225)),
                ('custmer', models.ForeignKey(to='customer.Customer')),
            ],
        ),
    ]
