# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0019_auto_20161123_1531'),
    ]

    operations = [
        migrations.AddField(
            model_name='creditcard',
            name='brand',
            field=models.CharField(default=None, max_length=30, null=True),
        ),
    ]
