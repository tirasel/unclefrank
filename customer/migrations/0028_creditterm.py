# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0027_auto_20170328_1807'),
    ]

    operations = [
        migrations.CreateModel(
            name='CreditTerm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('customer_id', models.CharField(unique=True, max_length=30)),
                ('credit_term', models.DecimalField(default=0.0, null=True, max_digits=10, decimal_places=2)),
            ],
        ),
    ]
