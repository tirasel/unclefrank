# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0008_auto_20160829_0920'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='verification',
            field=models.CharField(max_length=5, null=True),
        ),
    ]
