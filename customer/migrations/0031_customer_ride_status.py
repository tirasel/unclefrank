# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0030_customer_is_otp_verification'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='ride_status',
            field=models.BooleanField(default=False),
        ),
    ]
