# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0021_customer_is_corporate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='contact_no',
            field=models.CharField(max_length=20),
        ),
    ]
