# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0005_creditcard_cvv'),
        ('customer', '0004_customer_picture'),
    ]

    operations = [
    ]
