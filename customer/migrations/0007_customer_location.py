# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0006_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='location',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
