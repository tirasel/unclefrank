# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0010_auto_20161017_1201'),
    ]

    operations = [
        migrations.AddField(
            model_name='creditcard',
            name='last_digits',
            field=models.CharField(default=None, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='creditcard',
            name='strite_id',
            field=models.CharField(default=None, max_length=225, null=True),
        ),
    ]
