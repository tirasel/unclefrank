# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0026_customercoupon'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customercoupon',
            name='user',
        ),
        migrations.DeleteModel(
            name='CustomerCoupon',
        ),
    ]
