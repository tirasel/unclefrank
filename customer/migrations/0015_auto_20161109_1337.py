# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0014_userprofile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='nric',
            field=models.CharField(default=None, max_length=50, blank=True),
        ),
    ]
