# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0029_customerfiles'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='is_otp_verification',
            field=models.BooleanField(default=False),
        ),
    ]
