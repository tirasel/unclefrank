# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0012_auto_20161018_1906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='contact_no',
            field=models.CharField(unique=True, max_length=20),
        ),
    ]
