from django.db import models
from base.models import BaseModel
from django.contrib.auth.models import User
from driver.models import Driver
from django.template.defaultfilters import slugify
from django.core.files import File
import os
import urllib

# Create your models here.

SUBJECTS = (
    ('Subject-1', 'Subject-1'),
    ('Subject-2', 'Subject-2'),
    ('Subject-3', 'Subject-3'),
    ('Subject-4', 'Subject-4'),
    ('Subject-5', 'Subject-5'),
)

def get_avarage_rattings_customer(customer):
    from service.models import CustomerRating
    rating = CustomerRating.objects.filter(customer=customer)
    if rating.count() == 0:
        avg_rating = 0
    else:
        total_rating = 0
        for rate in rating:
            total_rating = total_rating + int(rate.rating)
        avg_rating = total_rating / rating.count()
        avg_rating = float("{0:.1f}".format(avg_rating))
    return avg_rating


class Customer(BaseModel):
	user = models.OneToOneField(User, related_name = 'user')
	name = models.CharField(max_length=255, default=None, null=True)
	email = models.CharField(max_length=255)
	address = models.TextField(blank=False, null=True)
	postal_code = models.CharField(max_length=255, default=None, null=True)
	contact_no = models.CharField(max_length=20, unique=False)
	# nric = models.CharField(max_length=50,default=None, blank=True)
	slug = models.SlugField(default=None)
	picture = models.ImageField(upload_to='profile_images', blank=True)
	location = models.CharField(max_length=255, default="Singapore (SGD)", null=True)
	verification = models.CharField(max_length=5, null=True)
	#device token for push notification
	device_token = models.CharField(max_length=225, null=True, default=None)
	device_id = models.CharField(max_length=225, null=True, default=None)
	fav_driver = models.ManyToManyField(Driver)
	#for corporete drivers
	is_corporate = models.BooleanField(default=False)
	is_otp_verification = models.BooleanField(default=False)
	ride_status = models.BooleanField(default=False)
	def __unicode__(self):
		return self.name

	def save(self, **kwargs):
		self.slug = slugify(self.name + str(self.user.pk))
		super(Customer, self).save()

	def cutomer_rating(self):
		return str(get_avarage_rattings_customer(self))

	def add_profile_image_form_url(self, url):
		if not self.picture:
			result = urllib.urlretrieve(url)
			self.picture = File(open(result[0]))
		self.save()

	def current_ride_job(self):
		from service.models import PickupService
		if self.ride_status:
			try:
				services = PickupService.objects.filter(customer=self).order_by('-id')
				return services[0].id
			except:
				return None
		else:
			return None

	def current_ride_job_driver_id(self):
		from service.models import PickupService
		if self.ride_status:
			try:
				services = PickupService.objects.filter(customer=self).order_by('-id')
				return services[0].pickedup_by.id
			except:
				return None
		else:
			return None

	def courier_gst(self):
		from service.models import CourierGST
		gst = CourierGST.objects.all().order_by('-id')
		try:
			return str(gst[0].price)
		except:
			return str(0)

	def ride_ready(self):
		return False





class ContactUs(models.Model):
	first_name = models.CharField(max_length=120)
	last_name = models.CharField(max_length=120)
	email_address = models.EmailField()
	company = models.CharField(max_length=120)
	country = models.CharField(max_length=120)
	phone = models.CharField(max_length=120)
	subjects = models.CharField(max_length=3, choices=SUBJECTS, null=True)
	message = models.TextField(blank=False)
	track_id = models.CharField(max_length=120)

	def __unicode__(self):
		return self.email_address

class CreditCard(models.Model):
	custmer = models.ForeignKey(Customer)
	name = models.CharField(max_length=225)
	card_no = models.CharField(max_length=225, null=True, default=None)
	contact_no = models.CharField(max_length=225, null=True, default=None)
	expiry_date = models.CharField(max_length=225, null=True, default=None)
	cvv = models.CharField(max_length=225, null=True, default=None)
	external_reff_no = models.CharField(max_length=225, null=True, default=None)
	strite_id = models.CharField(max_length=225, null=True, default=None)
	last_digits = models.CharField(max_length=4, null=True, default=None)
	brand = models.CharField(max_length=30, null=True, default=None)
	def __unicode__(self):
		return self.name

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    phone_number = models.CharField(max_length=255, unique=False, null=True, default=None)

class CreditTerm(models.Model):
	customer_id = models.CharField(max_length=30, unique=True)
	credit_term = models.DecimalField(default=0.00, null=True, decimal_places=2, max_digits=10)

class CustomerFiles(BaseModel):
	custmer = models.ForeignKey(Customer)
	file_name = models.CharField(max_length=255)
	file_description = models.TextField(blank=True)
	file = models.FileField(upload_to='customer/%Y/%m/%d/')