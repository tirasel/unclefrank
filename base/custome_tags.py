from django import template

custome_tags = template.Library()

@custome_tags.get_type
def get_type(value):
    t = type(value)
    return t.__module__ + "." + t.__name__