from django.conf import settings
from django.core.signing import TimestampSigner, BadSignature
from django.core.urlresolvers import reverse


def sign_email(email):
    secret = TimestampSigner(settings.SECRET_KEY)
    email_hash = secret.sign(email)
    return email_hash

def unsign_email_hash(email_hash):
    secret = TimestampSigner(settings.SECRET_KEY)
    try:
        email = secret.unsign(email_hash, settings.MAX_AGE_HASH)
    except BadSignature:
        return None
    return email

def generate_unique_reset_password_link(user):
    email_hash = sign_email(user.email)
    link = 'http://' + settings.CURRENT_HOST + str(reverse('reset-password-form',
                                                           args=(email_hash,)))
    return link