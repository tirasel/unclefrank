from django.db import models
from django.contrib import auth


# Create your models here.


class AuditTrile(models.Model):
    table = models.CharField(max_length=255, default=None, null=True)
    old_data = models.TextField(default=None, null=True)
    new_data = models.TextField(default=None, null=True)
    operation = models.CharField(max_length=255, default=None, null=True)
    user_name = models.CharField(max_length=255, default=None, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)


class BaseModel(models.Model):
    created_by = models.ForeignKey("auth.User", blank=True, null=True, related_name="+")
    updated_by = models.ForeignKey("auth.User", blank=True, null=True, related_name="+")
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    status = models.BooleanField(default=True)

    class Meta:
        abstract = True

