from django.conf.urls import include,url

urlpatterns = [
    url(r'^about/$', 'base.views.about', name="about"),
    url(r'^process/$', 'base.views.service', name="process"),
    url(r'^join_us/$', 'base.views.join_us', name="joinus"),
    url(r'^corporate/$', 'base.views.corporate', name="corporate"),
    url(r'^contact_us/$', 'base.views.contact_us', name="contact_us"),
    url(r'^career/$', 'base.views.career', name="career"),
    url(r'^privacy/$', 'base.views.privacy', name="privacy"),
    url(r'^tnc/$', 'base.views.tnc', name="tnc"),
    url(r'^tnc_mobile/$', 'base.views.tnc_mobile', name="tnc_mobile"),
    url(r'^faq/$', 'base.views.faq', name="faq"),


    ]