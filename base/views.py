from django.shortcuts import render, render_to_response
from django.template import RequestContext
from customer.forms import ContactUsForm, ContactUsFormNew
from django.http import HttpResponse
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
from superadmin.models import SliderImage
from service.models import VehicleType, PackageSize
from django.views.decorators.cache import cache_page
from superadmin.models import HomePageSliderCourrier, HomePageSliderRide



@cache_page(3600*24*1)
def home(request):
	arg = {}
	arg['vehicle'] = VehicleType.objects.all()
	arg['package'] = PackageSize.objects.all()
	arg['courier_sliders'] = HomePageSliderCourrier.objects.all()
	arg['pickup_sliders'] = HomePageSliderRide.objects.all()
	return render_to_response('frontend/index.html', arg, context_instance=RequestContext(request))

def veri(request):
	return HttpResponse("f562ab3264ec8f3502571c8cfb56f5b0c673bdf17819790d5ba5bbeb2063c90c")

# Create your views here.
@cache_page(3600*24*1)
def about(request):
	arg = {}
	arg['about'] = True
	return render_to_response('frontend/about.html', arg, context_instance=RequestContext(request))	

@cache_page(3600*24*1)
def service(request):
	arg = {}
	arg['service'] = True
	return render_to_response('frontend/process.html', arg, context_instance=RequestContext(request))
@cache_page(3600*24*1)
def join_us(request):
	arg = {}
	arg['join_us_page'] = True
	return render_to_response('frontend/join_us.html', arg, context_instance=RequestContext(request))
@cache_page(3600*24*1)
def corporate(request):
	arg = {}
	arg['corporate'] = True
	return render_to_response('frontend/corporate.html', arg, context_instance=RequestContext(request))
@cache_page(3600*24*1)
def privacy(request):
	arg = {}
	return render_to_response('base/pages/privacy.html', arg, context_instance=RequestContext(request))
@cache_page(3600*24*1)
def tnc(request):
	arg = {}
	return render_to_response('base/pages/tnc.html', arg, context_instance=RequestContext(request))

# @cache_page(3600*24*1)
def tnc_mobile(request):
	arg = {}
	return render_to_response('base/pages/tnc_mobile.html', arg, context_instance=RequestContext(request))


@cache_page(3600*24*1)
def faq(request):
	arg = {}
	return render_to_response('base/pages/faq.html', arg, context_instance=RequestContext(request))

@cache_page(3600*24*1)
def contact_us(request):
	arg = {}
	form = ContactUsForm()
	if request.method == 'POST':
		form = ContactUsForm(request.POST)
		if form.is_valid():
			email_address = form.cleaned_data['email_address']
			message = form.cleaned_data['message']
			Subject = 'UncleFrank contact form'
			from_email = 'uncle.frank2020@gmail.com'
			content_email = "Dear Sir, Your email has been received. Here is your email %s" %(message)
			to_email = ['admin@unclefrank.com.sg']#['ridwan_rahman@ymail.com']
			send_mail(
    			Subject,
    			content_email,
    			from_email,
    			to_email,
    			fail_silently=True,
				)
			return render_to_response('frontend/contactus.html', arg, context_instance=RequestContext(request))

	arg = {'form': form}
	arg['contact_us'] = True
	return render_to_response('frontend/contactus.html', arg, context_instance=RequestContext(request))
def career(request):
	arg = {}
	return render_to_response('base/pages/career.html', arg, context_instance=RequestContext(request))
	

