from superadmin.models import HomeCMS, AboutPage, Services, JoinUs, Careers, Corporate, ContactUsPage, SliderImage, Terms, Privacy, Faq, AccessControl
from customer.models import Customer
from django.core.urlresolvers import resolve
from superadmin.models import CommonCms
from django.http import HttpResponse


def context(request):
	arg = {}
	url_name = ""
	try:
		commons = CommonCms.objects.all()
		arg['common'] = commons[0]
	except:
		arg['common'] = None
	try:
		arg['customer'] = Customer.objects.get(user=request.user)
	except:
		arg['customer'] = None
	try:
		url_name = resolve(request.path_info).url_name
	except:
		url_name = ""
	if url_name == "home":
		try:
			home = HomeCMS.objects.all()
			arg['home'] = home[0]
		except:
			home = None
	elif url_name == "about":
		try:
			about = AboutPage.objects.all()
			slider = SliderImage.objects.all()
			arg['about_page'] = about[0]
			arg['slider'] = slider
		except:
			arg['about_page'] = None
			arg['slider'] = None
	elif url_name == "process":
		try:
			services = Services.objects.all()
			arg['services'] = services[0]
		except:
			arg['services'] = None
	elif url_name == "joinus":
		try:
			joinus = JoinUs.objects.all()
			arg['joinus'] = joinus[0]
		except:
			arg['joinus'] = None
	elif url_name == "corporate":
		try:
			corporate = Corporate.objects.all()
			arg['corporate_page'] = corporate[0]
		except:
			arg['corporate_page'] = None
	elif url_name == "career":
		try:
			careers = Careers.objects.all()
			arg['careers'] = careers[0]
		except:
			arg['careers'] = None
	elif url_name == "privacy":
		try:
			arg['privacy'] = Privacy.objects.all()[0]
		except:
			arg['privacy'] = None
	elif url_name == "tnc" or url_name == "tnc_mobile":
		try:
			arg['terms'] = Terms.objects.all()[0]
		except:
			arg['terms'] = None
	elif url_name == "faq":
		try:
			arg['faq'] = Faq.objects.all()[0]
		except:
			arg['faq'] = None
	elif url_name == "contact_us":
		try:
			contactus = ContactUsPage.objects.all()
			arg['contactus'] = contactus[0]
		except:
			arg['contactus'] = None
	return arg

def access(request):
	arg = {}
	try:
		arg['permissions'] = AccessControl.objects.get(user=request.user)
	except Exception, e:
		arg['permissions'] = None

 #    # for testing only
 	# permission = {}
 	# permission['is_cms'] = True
 	# permission['is_settings'] = True
 	# permission['is_courier'] = True
 	# permission['is_pickup'] = True
 	# permission['is_driver_info'] = True
 	# permission['is_sales_report'] = True
 	# permission['is_customer_list'] = True
 	# permission['is_account'] = True
 	# permission['is_add_coupon'] = True
 	# permission['is_window_setup'] = True
 	# permission['is_address_setup'] = True
 	# arg = {
 	# 	'permissions': permission
 	# 	}
	return arg
