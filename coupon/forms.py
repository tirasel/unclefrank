from django import forms
from superadmin.models import ExpressWindows
from .models import Coupon, PickupCoupon
from django.core.exceptions import ValidationError



COUPON_TYPE = (
    ('onetime', 'onetime'),
    ('multi', 'multi'),
)

DISCOUNT_TYPE = (
    ('amount', 'amount'),
    ('percentage', 'percentage'),
)

PERIOD = (
    ('MORNING', 'MORNING'),
    ('AFTERNOON', 'AFTERNOON'),
	('EVENING', 'EVENING'),
	('NIGHT', 'NIGHT'),
)



class CouponForm(forms.ModelForm):
	# code_type = forms.ChoiceField(choices=COUPON_TYPE, widget=forms.Select(attrs={'class': 'form-control select-styled site-field',
	# 																			  'required': 'required'}))
	code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'code'}))
	delivery_window = forms.ModelChoiceField(queryset= ExpressWindows.objects.all(), required=False, empty_label="(All)", widget=forms.Select(attrs={'class': 'form-control select-styled site-field'}))
	start_date =  forms.DateField(required=True, widget=forms.widgets.DateInput(format='%m/%d/%y', attrs={'class': 'form-control select-styled site-field date'}))
	start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time'}))
	end_date =  forms.DateField(required=True, widget=forms.widgets.DateInput(format='%m/%d/%y', attrs={'class': 'form-control select-styled site-field date'}))
	discount_type = forms.ChoiceField(choices=DISCOUNT_TYPE, widget=forms.Select(attrs={'class': 'form-control select-styled site-field'}))
	end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time'}))
	discount_value = forms.DecimalField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
	number_of_use = forms.IntegerField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
	number_of_use_per_user = forms.IntegerField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control huge'}))

	class Meta:
		model = Coupon
		fields = ['code', 'delivery_window', 'start_date', 'start_time', 'end_date', 'end_time', 'discount_type', 'discount_value', 'number_of_use', 'number_of_use_per_user']

	# def clean_end_date(self):
	# 	if self.cleaned_data['start_date'] > self.cleaned_data['end_date']:
	# 		raise ValidationError("End Date must be grater than start Date and time")
	# 	return self.cleaned_data['end_date']

	def clean(self):
		cleaned_data = super(CouponForm, self).clean()
		if cleaned_data.get('code') == None or cleaned_data.get('start_date') == None or cleaned_data.get('start_time') == None or cleaned_data.get('end_date') == None or cleaned_data.get('end_time') == None or cleaned_data.get('discount_value') == None or cleaned_data.get('number_of_use') == None or cleaned_data.get('number_of_use_per_user') == None:
			# raise ValidationError("All Field required")
			pass
		else:
			if cleaned_data.get('start_date') == cleaned_data.get('end_date') and cleaned_data.get('end_time') <= cleaned_data.get('start_time'):
				raise ValidationError("End Date and time must be grater than start Date and time")
			elif cleaned_data.get('start_date') > cleaned_data.get('end_date'):
				raise ValidationError("End Date and time must be grater than start Date and time")
		return cleaned_data
	
	# def clean_end_time(self):
	# 	if self.cleaned_data['start_date'] == self.cleaned_data['end_date'] and self.cleaned_data['end_time'] <= self.cleaned_data['start_time']:
	# 		raise ValidationError("End Date and time must be grater than start Date and time")
	# 	elif self.cleaned_data['start_date'] > self.cleaned_data['end_date']:
	# 		raise ValidationError("End Date and time must be grater than start Date and time")
	# 	return self.cleaned_data['end_time']


class PickupCouponForm(forms.ModelForm):
	# code_type = forms.ChoiceField(choices=COUPON_TYPE, widget=forms.Select(attrs={'class': 'form-control select-styled site-field',
	# 																			  'required': 'required'}))
	code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'code'}))
	# delivery_window = forms.ModelChoiceField(queryset= ExpressWindows.objects.all(), required=False, empty_label="(All)", widget=forms.Select(attrs={'class': 'form-control select-styled site-field'}))
	start_date =  forms.DateField(required=True, widget=forms.widgets.DateInput(format='%m/%d/%y', attrs={'class': 'form-control select-styled site-field date'}))
	start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time', 'required': 'required'}))
	end_date =  forms.DateField(required=True, widget=forms.widgets.DateInput(format='%m/%d/%y', attrs={'class': 'form-control select-styled site-field date',
																						 'required': 'required'}))
	discount_type = forms.ChoiceField(required=True, choices=DISCOUNT_TYPE, widget=forms.Select(attrs={'class': 'form-control select-styled site-field',
																						'required': 'required'}))
	end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time', 'required': 'required'}))
	discount_value = forms.DecimalField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
	number_of_use = forms.IntegerField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
	number_of_use_per_user = forms.IntegerField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control huge'}))

	class Meta:
		model = PickupCoupon
		fields = ['code', 'start_date', 'start_time', 'end_date', 'end_time', 'discount_type', 'discount_value', 'number_of_use', 'number_of_use_per_user']

	# def clean_end_time(self):
	# 	if self.cleaned_data['start_date'] == self.cleaned_data['end_date'] and self.cleaned_data['end_time'] <= self.cleaned_data['start_time']:
	# 		raise ValidationError("End Date and time must be grater than start Date and time")
	# 	elif self.cleaned_data['start_date'] > self.cleaned_data['end_date']:
	# 		raise ValidationError("End Date and time must be grater than start Date and time")
	# 	return self.cleaned_data['end_time']

	def clean(self):
		cleaned_data = super(PickupCouponForm, self).clean()
		if cleaned_data.get('code') == None or cleaned_data.get('start_date') == None or cleaned_data.get('start_time') == None or cleaned_data.get('end_date') == None or cleaned_data.get('end_time') == None or cleaned_data.get('discount_value') == None or cleaned_data.get('number_of_use') == None or cleaned_data.get('number_of_use_per_user') == None:
			# raise ValidationError("All Field required")
			pass
		else:
			if cleaned_data.get('start_date') == cleaned_data.get('end_date') and cleaned_data.get('end_time') <= cleaned_data.get('start_time'):
				raise ValidationError("End Date and time must be grater than start Date and time")
			elif cleaned_data.get('start_date') > cleaned_data.get('end_date'):
				raise ValidationError("End Date and time must be grater than start Date and time")
		return cleaned_data
	
