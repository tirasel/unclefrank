from django.db import models
from base.models import BaseModel
from superadmin.models import ExpressWindows
from customer.models import Customer
from django.contrib.auth.models import User
# Create your models here.

COUPON_TYPY = (
    ('onetime', 'onetime'),
    ('multi', 'multi'),
)

DISCOUNT_TYPE = (
    ('amount', 'amount'),
    ('percentage', 'percentage'),
)
JOB_TYPE = (
	('delivery', 'delivery'),
	('courier', 'courier')
	)

class Coupon(BaseModel):
	code = models.CharField(max_length=10, default=None, null=True)
	code_type = models.CharField(max_length=25, choices=COUPON_TYPY, default='multi', null=True)
	start_date = models.DateField(blank=False)
	end_date = models.DateField(blank=False)
	start_time = models.TimeField(null=True, default=None)
	end_time = models.TimeField(null=True, default=None)
	discount_type = models.CharField(max_length=25, choices=DISCOUNT_TYPE, default=None, null=True)
	discount_value = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)
	number_of_use = models.IntegerField(default=0)
	number_of_use_per_user = models.IntegerField(default=10)
	is_user = models.BooleanField(default=False)
	period = models.CharField(max_length=50, null=True)
	window = models.CharField(max_length=50, null=True)
	delivery_window = models.ForeignKey(ExpressWindows, default=None, null=True)
	
	def __unicode__(self):
		return self.code

	def is_valid(self, customer, window=""):
		from datetime import datetime
		if self.window and window != self.window:
			return False
		start_date_time = datetime.combine(self.start_date, self.start_time)
		end_date_time = datetime.combine(self.end_date, self.end_time)
		current_date_time = datetime.now()
		if start_date_time > current_date_time or current_date_time > end_date_time:
			return False
		number_of_use_per_user = self.number_of_use_per_user
		no_of_redumption = CouponRedemptions.objects.filter(used_by=customer, cupon_code=self.code).count()
		if no_of_redumption >= number_of_use_per_user:
			return False
		else:
			return True

	def get_discount_amount(self, cost):
		if self.discount_type == 'percentage':
			discount = round(((cost/100) * float(self.discount_value)), 2)
		else:
			discount = float(self.discount_value)
		if discount >= cost:
			discount = 0
		return discount

	def use_coupon(self, customer, job_id, discount_value):
		redumption = CouponRedemptions()
		redumption.used_by = customer
		redumption.job_id = job_id
		redumption.discount_value = discount_value
		redumption.save()
		return True

class CouponRedemptions(BaseModel):
	cupon_code = models.CharField(max_length=10, default=None, null=True)
	used_by = models.ForeignKey(Customer, default=None, null=True)
	job_id = models.IntegerField(default=0)
	job_type = models.CharField(max_length=6, default=None, null=True)
	discount_value = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)


class PickupCoupon(BaseModel):
	code = models.CharField(max_length=10, default=None, null=True)
	code_type = models.CharField(max_length=25, choices=COUPON_TYPY, default='multi', null=True)
	start_date = models.DateField(blank=False)
	end_date = models.DateField(blank=False)
	start_time = models.TimeField(null=True, default=None)
	end_time = models.TimeField(null=True, default=None)
	discount_type = models.CharField(max_length=25, choices=DISCOUNT_TYPE, default=None, null=True)
	discount_value = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)
	number_of_use = models.IntegerField(default=0)
	number_of_use_per_user = models.IntegerField(default=10)
	is_user = models.BooleanField(default=False)
	period = models.CharField(max_length=50, null=True)
	# delivery_window = models.ForeignKey(ExpressWindows, default=None, null=True)

	def __unicode__(self):
		return self.code

	def is_valid(self, customer, window=""):
		from datetime import datetime
		start_date_time = datetime.combine(self.start_date, self.start_time)
		end_date_time = datetime.combine(self.end_date, self.end_time)
		current_date_time = datetime.now()
		if start_date_time > current_date_time or current_date_time > end_date_time:
			return False
		number_of_use_per_user = self.number_of_use_per_user
		no_of_redumption = RideCouponRedemptions.objects.filter(used_by=customer, cupon_code=self.code).count()
		if no_of_redumption >= number_of_use_per_user:
			return False
		else:
			return True

	def get_discount_amount(self, cost):
		if self.discount_type == 'percentage':
			discount = round(((cost/100) * float(self.discount_value)), 2)
		else:
			discount = float(self.discount_value)
		if discount >= cost:
			discount = 0
		return discount

	def use_coupon(self, customer, job_id, discount_value):
		redumption = RideCouponRedemptions()
		redumption.used_by = customer
		redumption.job_id = job_id
		redumption.discount_value = discount_value
		redumption.save()
		return True

class RideCouponRedemptions(BaseModel):
	cupon_code = models.CharField(max_length=10, default=None, null=True)
	used_by = models.ForeignKey(Customer, default=None, null=True)
	job_id = models.IntegerField(default=0)
	job_type = models.CharField(max_length=6, default=None, null=True)
	discount_value = models.DecimalField(max_digits=10, decimal_places=2,default=0.00)


class CustomerCoupon(models.Model):
	user = models.ForeignKey(User)
	number_of_use = models.IntegerField()
	code = models.CharField(max_length=20, null=True)
	coupon = models.ForeignKey(PickupCoupon)