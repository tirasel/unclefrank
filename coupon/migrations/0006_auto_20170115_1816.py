# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0005_coupon_delivery_window'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='end_time',
            field=models.TimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='coupon',
            name='start_time',
            field=models.TimeField(default=None, null=True),
        ),
    ]
