# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0007_couponredemptions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coupon',
            name='code_type',
            field=models.CharField(default=b'multi', max_length=25, null=True, choices=[(b'onetime', b'onetime'), (b'multi', b'multi')]),
        ),
    ]
