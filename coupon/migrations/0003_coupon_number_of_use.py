# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0002_auto_20161228_1408'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='number_of_use',
            field=models.IntegerField(default=0),
        ),
    ]
