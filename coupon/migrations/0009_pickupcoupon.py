# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('superadmin', '0051_expresswindows_end_time'),
        ('coupon', '0008_auto_20170207_1324'),
    ]

    operations = [
        migrations.CreateModel(
            name='PickupCoupon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('status', models.BooleanField(default=True)),
                ('code', models.CharField(default=None, max_length=6, null=True)),
                ('code_type', models.CharField(default=b'multi', max_length=25, null=True, choices=[(b'onetime', b'onetime'), (b'multi', b'multi')])),
                ('start_date', models.DateField(blank=True)),
                ('end_date', models.DateField(blank=True)),
                ('start_time', models.TimeField(default=None, null=True)),
                ('end_time', models.TimeField(default=None, null=True)),
                ('discount_type', models.CharField(default=None, max_length=25, null=True, choices=[(b'amount', b'amount'), (b'percentage', b'percentage')])),
                ('discount_value', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('number_of_use', models.IntegerField(default=0)),
                ('is_user', models.BooleanField(default=False)),
                ('period', models.CharField(max_length=50, null=True)),
                ('created_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('delivery_window', models.ForeignKey(default=None, to='superadmin.ExpressWindows', null=True)),
                ('updated_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
