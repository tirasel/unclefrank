# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0009_pickupcoupon'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pickupcoupon',
            name='delivery_window',
        ),
    ]
