# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customer', '0021_customer_is_corporate'),
        ('coupon', '0006_auto_20170115_1816'),
    ]

    operations = [
        migrations.CreateModel(
            name='CouponRedemptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('status', models.BooleanField(default=True)),
                ('cupon_code', models.CharField(default=None, max_length=6, null=True)),
                ('job_id', models.IntegerField(default=0)),
                ('job_type', models.CharField(default=None, max_length=6, null=True)),
                ('discount_value', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('created_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('updated_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('used_by', models.ForeignKey(default=None, to='customer.Customer', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
