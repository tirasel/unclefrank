# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0015_auto_20170713_2034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coupon',
            name='end_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='start_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='pickupcoupon',
            name='end_date',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='pickupcoupon',
            name='start_date',
            field=models.DateField(),
        ),
    ]
