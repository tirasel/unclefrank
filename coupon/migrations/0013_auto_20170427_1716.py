# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0012_customercoupon_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='number_of_use_per_user',
            field=models.IntegerField(default=10),
        ),
        migrations.AddField(
            model_name='pickupcoupon',
            name='number_of_use_per_user',
            field=models.IntegerField(default=10),
        ),
    ]
