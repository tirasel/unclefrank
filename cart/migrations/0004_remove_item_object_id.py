# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0003_remove_item_content_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='object_id',
        ),
    ]
