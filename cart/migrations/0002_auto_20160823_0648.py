# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0002_packagesize'),
        ('cart', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='height',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='item',
            name='image',
            field=models.ImageField(null=True, upload_to=b'cart', blank=True),
        ),
        migrations.AddField(
            model_name='item',
            name='length',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='item',
            name='size',
            field=models.ForeignKey(to='service.PackageSize', null=True),
        ),
        migrations.AddField(
            model_name='item',
            name='weight',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='item',
            name='width',
            field=models.IntegerField(default=0),
        ),
    ]
