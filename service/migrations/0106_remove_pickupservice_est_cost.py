# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0105_pickupservice_est_cost'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pickupservice',
            name='est_cost',
        ),
    ]
