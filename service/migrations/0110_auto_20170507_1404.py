# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0109_auto_20170504_1417'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='is_cradite_term',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='is_cradite_term_billed',
            field=models.BooleanField(default=False),
        ),
    ]
