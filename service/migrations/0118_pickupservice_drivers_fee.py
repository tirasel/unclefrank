# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0117_craditetermpayment_customer'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='drivers_fee',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
    ]
