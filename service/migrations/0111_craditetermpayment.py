# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0110_auto_20170507_1404'),
    ]

    operations = [
        migrations.CreateModel(
            name='CraditeTermPayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cost', models.DecimalField(default=0.0, null=True, max_digits=10, decimal_places=2)),
                ('express_charge', models.DecimalField(default=0.0, null=True, max_digits=10, decimal_places=2)),
                ('sercharge', models.DecimalField(default=0.0, null=True, max_digits=10, decimal_places=2)),
                ('discount', models.DecimalField(default=0.0, null=True, max_digits=10, decimal_places=2)),
                ('total', models.DecimalField(default=0.0, null=True, max_digits=10, decimal_places=2)),
                ('admin_fee', models.DecimalField(default=0.0, null=True, max_digits=10, decimal_places=2)),
                ('is_paid', models.BooleanField(default=False)),
                ('date_time', models.DateTimeField(auto_now=True)),
                ('delevery_service', models.ForeignKey(default=None, to='service.DeliveryService', null=True)),
            ],
        ),
    ]
