# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0020_activitylog_logger_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='distance',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='eta',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
