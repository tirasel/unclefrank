# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0078_pickupsurgecharge_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimeCharge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=120, null=True)),
                ('start_time', models.TimeField(default=None, null=True)),
                ('end_time', models.TimeField(default=None, null=True)),
                ('price', models.DecimalField(default=0.0, max_digits=7, decimal_places=2)),
            ],
        ),
    ]
