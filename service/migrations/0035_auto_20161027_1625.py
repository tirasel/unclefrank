# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0034_auto_20161026_2317'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='dropoff_address_lat',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='dropoff_address_lng',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='pickup_address_lat',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='pickup_address_lng',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
