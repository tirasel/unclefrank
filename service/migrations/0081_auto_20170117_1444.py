# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0080_couriersurgecharge'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='TimeCharge',
            new_name='PickupTimeCharge',
        ),
    ]
