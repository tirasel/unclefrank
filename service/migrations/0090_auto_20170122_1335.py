# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0089_auto_20170118_1522'),
    ]

    operations = [
        migrations.AddField(
            model_name='couriersurgecharge',
            name='corp_charge',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='pickupsurgecharge',
            name='corp_charge',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
    ]
