# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0054_deliveryservice_payment_status'),
        ('service', '0054_adminmessage_created_at'),
    ]

    operations = [
    ]
