# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0074_pickupservice_order_no'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='is_archived',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='is_archived',
            field=models.BooleanField(default=False),
        ),
    ]
