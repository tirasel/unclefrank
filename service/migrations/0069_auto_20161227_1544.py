# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import service.models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0068_auto_20161218_1727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='image',
            field=models.ImageField(upload_to=service.models.content_file_name),
        ),
    ]
