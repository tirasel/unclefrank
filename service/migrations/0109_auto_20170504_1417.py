# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0108_auto_20170504_1414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='collection_postal_code',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AlterField(
            model_name='deliveryservice',
            name='delivery_postal_code',
            field=models.CharField(max_length=120, null=True),
        ),
    ]
