# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0032_auto_20161023_1834'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicletype',
            name='pic_active',
            field=models.ImageField(default=None, null=True, upload_to=b'sadmin/vehicle_edit/', blank=True),
        ),
    ]
