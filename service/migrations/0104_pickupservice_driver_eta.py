# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0103_auto_20170405_1441'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='driver_eta',
            field=models.IntegerField(null=True),
        ),
    ]
