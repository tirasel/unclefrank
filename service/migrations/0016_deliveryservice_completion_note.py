# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0015_activitylog'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='completion_note',
            field=models.TextField(default=None, null=True),
        ),
    ]
