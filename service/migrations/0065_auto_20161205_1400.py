# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0064_payment_job_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tracking',
            name='datetime',
            field=models.DateTimeField(null=True),
        ),
    ]
