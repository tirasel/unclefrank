# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0119_deliveryservice_auth_capture_token'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='created_date',
            field=models.DateField(default=datetime.datetime(2017, 7, 24, 11, 44, 4, 291621, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='created_date',
            field=models.DateField(default=datetime.datetime(2017, 7, 24, 11, 44, 20, 827622, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
