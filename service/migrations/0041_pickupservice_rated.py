# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0040_auto_20161101_1753'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='rated',
            field=models.BooleanField(default=False),
        ),
    ]
