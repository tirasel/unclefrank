# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0018_deliveryservice_cancell_note'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitylog',
            name='delevery_status',
            field=models.CharField(default=b'current', max_length=255, choices=[(b'current', b'current'), (b'taken', b'taken'), (b'completed', b'completed'), (b'cancelled', b'cancelled')]),
        ),
    ]
