# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0010_driver_driver_type'),
        ('customer', '0011_auto_20161018_1404'),
        ('service', '0028_tracking'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField()),
                ('comment', models.CharField(max_length=220, null=True)),
                ('customer', models.ForeignKey(to='customer.Customer')),
                ('rated_by', models.ForeignKey(to='driver.Driver')),
            ],
        ),
        migrations.CreateModel(
            name='DriverRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField()),
                ('comment', models.CharField(max_length=220, null=True)),
                ('driver', models.ForeignKey(to='driver.Driver')),
                ('rated_by', models.ForeignKey(to='customer.Customer')),
            ],
        ),
    ]
