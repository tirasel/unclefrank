# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0005_vehicletype'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pickupservice',
            name='vehicle',
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='delevery_status',
            field=models.CharField(default=b'pending', max_length=255),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='vehicle_type',
            field=models.ForeignKey(default=None, to='service.VehicleType'),
        ),
    ]
