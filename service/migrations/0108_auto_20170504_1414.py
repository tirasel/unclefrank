# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0107_auto_20170503_2000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='collection_postal_code',
            field=models.CharField(default=None, max_length=255),
        ),
        migrations.AlterField(
            model_name='deliveryservice',
            name='delivery_postal_code',
            field=models.CharField(default=None, max_length=255),
        ),
    ]
