# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0066_auto_20161205_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitylog',
            name='lat',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='activitylog',
            name='lng',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
