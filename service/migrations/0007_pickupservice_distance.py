# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0006_auto_20160829_1153'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='distance',
            field=models.TextField(null=True),
        ),
    ]
