# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0030_customer_is_otp_verification'),
        ('service', '0116_auto_20170717_2051'),
    ]

    operations = [
        migrations.AddField(
            model_name='craditetermpayment',
            name='customer',
            field=models.ForeignKey(default=None, to='customer.Customer', null=True),
        ),
    ]
