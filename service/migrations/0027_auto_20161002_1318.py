# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0026_auto_20160929_1245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pricing',
            name='schedule_type',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
