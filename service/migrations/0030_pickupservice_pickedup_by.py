# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('driver', '0012_auto_20161022_1328'),
        ('service', '0029_customerrating_driverrating'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='pickedup_by',
            field=models.ForeignKey(default=None, to='driver.Driver', null=True),
        ),
    ]
