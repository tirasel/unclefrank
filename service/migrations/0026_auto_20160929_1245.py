# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0025_pickuprate_pickupsurgecharge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pricing',
            old_name='rate',
            new_name='base_rate',
        ),
        migrations.RemoveField(
            model_name='pricing',
            name='carton_type',
        ),
        migrations.RemoveField(
            model_name='pricing',
            name='day_type',
        ),
        migrations.AddField(
            model_name='packagesize',
            name='end_weight',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='packagesize',
            name='start_weight',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='pricing',
            name='corp_base_rate',
            field=models.DecimalField(default=None, null=True, max_digits=5, decimal_places=2),
        ),
        migrations.AddField(
            model_name='pricing',
            name='corp_range_rate',
            field=models.DecimalField(default=None, null=True, max_digits=5, decimal_places=2),
        ),
        migrations.AddField(
            model_name='pricing',
            name='end_qty',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='pricing',
            name='package_size',
            field=models.ForeignKey(to='service.PackageSize', null=True),
        ),
        migrations.AddField(
            model_name='pricing',
            name='range_rate',
            field=models.DecimalField(default=None, null=True, max_digits=5, decimal_places=2),
        ),
        migrations.AddField(
            model_name='pricing',
            name='start_qty',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='pricing',
            name='schedule_type',
            field=models.CharField(default=None, max_length=255, null=True, choices=[(b'Same Day', b'Same Day'), (b'Next Day', b'Next Day'), (b'Two Working Days', b'Two Working Days')]),
        ),
    ]
