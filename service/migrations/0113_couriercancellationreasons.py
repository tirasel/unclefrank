# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0112_vehicletype_service_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourierCancellationReasons',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(default=None, null=True, upload_to=b'pickup/cancellation', blank=True)),
                ('image_active', models.ImageField(default=None, null=True, upload_to=b'pickup/cancellation', blank=True)),
                ('reason', models.CharField(max_length=120, null=True)),
            ],
        ),
    ]
