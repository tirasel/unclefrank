# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0016_deliveryservice_completion_note'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='job_cancelled_time',
            field=models.DateTimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='job_complete_time',
            field=models.DateTimeField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='job_start_time',
            field=models.DateTimeField(default=None, null=True),
        ),
    ]
