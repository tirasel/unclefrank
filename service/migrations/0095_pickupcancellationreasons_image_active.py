# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0094_pickupcancellationreasons'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupcancellationreasons',
            name='image_active',
            field=models.ImageField(default=None, null=True, upload_to=b'pickup/cancellation', blank=True),
        ),
    ]
