# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0060_auto_20161129_1519'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='admin_fee',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='payment',
            name='car_fee',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='payment',
            name='online_fee',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='payment',
            name='toll_fee',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
    ]
