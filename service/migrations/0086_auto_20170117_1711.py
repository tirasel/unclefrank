# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0085_couriergst'),
    ]

    operations = [
        migrations.AddField(
            model_name='couriertimecharge',
            name='postal_code',
            field=models.CharField(max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='pickuptimecharge',
            name='postal_code',
            field=models.CharField(max_length=500, null=True),
        ),
    ]
