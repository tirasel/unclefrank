# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0019_activitylog_delevery_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitylog',
            name='logger_name',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
    ]
