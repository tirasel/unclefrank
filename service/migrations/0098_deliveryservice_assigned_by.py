# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0097_deliveryservice_drivers_fee'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='assigned_by',
            field=models.CharField(default=None, max_length=20, null=True),
        ),
    ]
