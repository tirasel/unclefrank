# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0096_auto_20170129_1434'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='drivers_fee',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
    ]
