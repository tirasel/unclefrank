# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0034_auto_20161026_2317'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitylog',
            name='log_type',
            field=models.CharField(default=None, max_length=120, null=True),
        ),
    ]
