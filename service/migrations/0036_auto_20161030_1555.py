# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0035_auto_20161027_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='packagesize',
            name='height',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='packagesize',
            name='length',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='packagesize',
            name='weight',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='packagesize',
            name='width',
            field=models.IntegerField(default=0),
        ),
    ]
