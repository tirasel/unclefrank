# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0121_auto_20170803_1752'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='gst',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
    ]
