# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0021_customer_is_corporate'),
        ('driver', '0025_driver_is_corporate'),
        ('service', '0091_deliveryservice_is_corporate'),
    ]

    operations = [
        migrations.CreateModel(
            name='CorporateDriverCustomer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('customer', models.ForeignKey(to='customer.Customer')),
                ('driver', models.ForeignKey(to='driver.Driver')),
            ],
        ),
    ]
