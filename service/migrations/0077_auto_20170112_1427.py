# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0076_auto_20170112_1426'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='delevery_remarks',
            field=models.TextField(null=True),
        ),
    ]
