# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0012_deliveryservice_taken_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='total_cost',
            field=models.DecimalField(default=0.0, max_digits=10, decimal_places=2),
        ),
    ]
