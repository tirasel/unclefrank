# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0118_pickupservice_drivers_fee'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='auth_capture_token',
            field=models.CharField(max_length=120, null=True),
        ),
    ]
