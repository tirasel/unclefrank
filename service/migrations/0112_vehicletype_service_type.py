# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0111_craditetermpayment'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicletype',
            name='service_type',
            field=models.CharField(default=b'both', max_length=10, null=True, choices=[(b'courier', b'courier'), (b'pickup', b'pickup'), (b'both', b'both')]),
        ),
    ]
