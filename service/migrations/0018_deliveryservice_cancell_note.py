# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0017_auto_20160907_0843'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='cancell_note',
            field=models.TextField(default=None, null=True),
        ),
    ]
