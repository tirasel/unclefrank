# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0082_couriertimecharge'),
    ]

    operations = [
        migrations.CreateModel(
            name='gst',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=120, null=True)),
                ('percentage', models.BooleanField(default=False)),
                ('price', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
            ],
        ),
    ]
