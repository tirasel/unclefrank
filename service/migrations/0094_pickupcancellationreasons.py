# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0093_favoritedrivercustomer'),
    ]

    operations = [
        migrations.CreateModel(
            name='PickupCancellationReasons',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(default=None, null=True, upload_to=b'pickup/cancellation', blank=True)),
                ('reason', models.CharField(max_length=120, null=True)),
            ],
        ),
    ]
