# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0039_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerrating',
            name='delivery',
            field=models.ForeignKey(to='service.DeliveryService', null=True),
        ),
        migrations.AddField(
            model_name='customerrating',
            name='pickup',
            field=models.ForeignKey(to='service.PickupService', null=True),
        ),
        migrations.AddField(
            model_name='driverrating',
            name='delivery',
            field=models.ForeignKey(to='service.DeliveryService', null=True),
        ),
        migrations.AddField(
            model_name='driverrating',
            name='pickup',
            field=models.ForeignKey(to='service.PickupService', null=True),
        ),
    ]
