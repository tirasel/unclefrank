# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0022_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='CartonType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('caption', models.CharField(default=None, max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='DayType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('caption', models.CharField(default=None, max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pricing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rate', models.DecimalField(default=None, null=True, max_digits=5, decimal_places=2)),
                ('carton_type', models.ForeignKey(to='service.CartonType')),
                ('day_type', models.ForeignKey(to='service.DayType')),
            ],
        ),
        migrations.CreateModel(
            name='ScheduleType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('caption', models.CharField(default=None, max_length=255, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='package',
            name='unit_price',
            field=models.DecimalField(default=None, null=True, max_digits=20, decimal_places=2),
        ),
        migrations.AddField(
            model_name='vehicletype',
            name='pic',
            field=models.ImageField(default=None, null=True, upload_to=b'sadmin/vehicle_edit/', blank=True),
        ),
        migrations.AddField(
            model_name='pricing',
            name='schedule_type',
            field=models.ForeignKey(to='service.ScheduleType'),
        ),
    ]
