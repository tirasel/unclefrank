# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0014_userprofile'),
        ('driver', '0016_auto_20161026_2020'),
        ('service', '0049_auto_20161106_1508'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdminMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message_body', models.CharField(max_length=600, null=True)),
                ('delivery', models.ForeignKey(to='service.DeliveryService', null=True)),
                ('message_to_customer', models.ForeignKey(to='customer.Customer', null=True)),
                ('message_to_driver', models.ForeignKey(to='driver.Driver', null=True)),
                ('pickup', models.ForeignKey(to='service.PickupService', null=True)),
            ],
        ),
    ]
