# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0098_deliveryservice_assigned_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveryservice',
            name='assigned_by',
            field=models.CharField(default=None, max_length=120, null=True),
        ),
    ]
