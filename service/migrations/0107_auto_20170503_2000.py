# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0106_remove_pickupservice_est_cost'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='dropoff_postal_code',
            field=models.CharField(default=None, max_length=255),
        ),
        migrations.AddField(
            model_name='pickupservice',
            name='pickup_postal_code',
            field=models.CharField(default=None, max_length=255),
        ),
    ]
