# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0003_auto_20160828_0645'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='size',
            field=models.ForeignKey(default=None, to='service.PackageSize', null=True),
        ),
    ]
