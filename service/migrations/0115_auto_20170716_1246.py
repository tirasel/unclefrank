# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0114_deliveryservice_short_order_no'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vehicletype',
            name='service_type',
            field=models.CharField(default=b'courier', max_length=10, null=True, choices=[(b'courier', b'courier'), (b'pickup', b'pickup'), (b'both', b'both')]),
        ),
    ]
