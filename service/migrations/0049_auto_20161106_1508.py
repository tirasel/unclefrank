# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0014_userprofile'),
        ('driver', '0016_auto_20161026_2020'),
        ('service', '0048_cancellation_info'),
    ]

    operations = [
        migrations.CreateModel(
            name='CancellationInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cancelled_by', models.CharField(max_length=120, null=True, choices=[(b'admin', b'admin'), (b'customer', b'customer'), (b'driver', b'driver')])),
                ('customer', models.ForeignKey(to='customer.Customer')),
                ('delivery_job', models.ForeignKey(to='service.DeliveryService', null=True)),
                ('driver', models.ForeignKey(to='driver.Driver')),
                ('pickup_job', models.ForeignKey(to='service.PickupService', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='cancellation_info',
            name='customer',
        ),
        migrations.RemoveField(
            model_name='cancellation_info',
            name='delivery_job',
        ),
        migrations.RemoveField(
            model_name='cancellation_info',
            name='driver',
        ),
        migrations.RemoveField(
            model_name='cancellation_info',
            name='pickup_job',
        ),
        migrations.DeleteModel(
            name='cancellation_info',
        ),
    ]
