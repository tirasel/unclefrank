# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0101_vehicletype_pic_detail'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupservice',
            name='coupone',
            field=models.CharField(default=None, max_length=120, null=True),
        ),
    ]
