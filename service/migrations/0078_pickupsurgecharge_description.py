# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0077_auto_20170112_1427'),
    ]

    operations = [
        migrations.AddField(
            model_name='pickupsurgecharge',
            name='description',
            field=models.CharField(default=None, max_length=500, null=True),
        ),
    ]
