# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0100_auto_20170306_1249'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicletype',
            name='pic_detail',
            field=models.ImageField(default=None, null=True, upload_to=b'sadmin/vehicle_edit/', blank=True),
        ),
    ]
