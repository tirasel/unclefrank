# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0036_activitylog_pickup_status'),
        ('service', '0035_auto_20161027_1625'),
    ]

    operations = [
    ]
