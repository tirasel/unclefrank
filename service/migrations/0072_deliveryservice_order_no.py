# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0071_auto_20161228_1955'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='order_no',
            field=models.CharField(default=None, max_length=20, null=True),
        ),
    ]
