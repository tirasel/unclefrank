# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0067_auto_20161206_1509'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryservice',
            name='collection_postal_code',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='collection_unit_number',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='delivery_postal_code',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AddField(
            model_name='deliveryservice',
            name='delivery_unit_number',
            field=models.CharField(max_length=120, null=True),
        ),
    ]
