# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0050_adminmessage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='driverrating',
            name='rating',
            field=models.DecimalField(null=True, max_digits=3, decimal_places=2),
        ),
    ]
