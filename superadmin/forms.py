from django import forms
from django.db import models
from .models import StringSetup, RotatingBanner, SiteConfigeration, HomeCMS, AboutPage, Services, JoinUs, Careers, Corporate, ContactUsPage, SliderImage, RegistrationBackground, DriverStatus, Faq, Terms, Privacy, ExpressWindows, CommissionSetup, DriverCategory, CustomerFaqPage
from service.models import CourierGST, PickupGST, CourierTimeCharge, PickupTimeCharge, PackageSize, VehicleType, CartonType, ScheduleType, DayType, Pricing, HolidayList, PickupSurgeCharge, PickUpRate, CourierSurgeCharge, PickupCancellationReasons, CourierCancellationReasons
from base import constant
from address.models import Postalcode
from .models import CommonCms
from .models import HomePageSliderCourrier
from .models import HomePageSliderRide
from customer.models import CreditTerm
from .models import PushMessage
from customer.models import Customer
from django.core.exceptions import ValidationError




SCHEDULE_TYPE = (
    ('Same Day','Same Day'),
    ('Next Day','Next Day'),
    ('Two Working Days','Two Working Days'))

class PushMessagerForm(forms.ModelForm):
    class Meta:
        model = PushMessage
        fields = '__all__'

class CreditTermForm(forms.ModelForm):
    customer_id = forms.CharField(required=False,  widget=forms.Select(choices=( (x.id, x.name+' ('+str(x.id)+')') for x in Customer.objects.all() ), attrs={'class': 'form-control huge'}))
    credit_term = forms.CharField(required=False, widget=forms.NumberInput(attrs={'class': 'form-control site-field'}))
    class Meta:
        model = CreditTerm
        fields = '__all__'
    
    def clean_credit_term(self):    
        if float(self.cleaned_data['credit_term']) <= 0:
            raise ValidationError("Amount should be positive number")
        return self.cleaned_data['credit_term']

class CommonCmsForm(forms.ModelForm):
    logo = forms.ImageField(label='Logo(197x75px)',required=False)
    logo_white_color = forms.ImageField(label='Logo White(197x75px)',required=False)
    youtube_link = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    facebook_link = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    twitter_link = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    instagram_link = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    android_app_download_link = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    iso_app_download_link = forms.CharField(label='iOS app download link', required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    copyright_text = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    company_address = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    company_address_latitude = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    company_address_longitude = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    company_phone_no = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    company_fax_no = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    company_email = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    company_website = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    dashboard_payment_page_header_text = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control site-field'}))
    class Meta:
        model = CommonCms
        exclude = ['facebook_image', 'twitter_image', 'instagram_image', 'youtube_image']


class HomePageSliderCourrierForm(forms.ModelForm):
    image = forms.ImageField(label='Image(380x684px)',required=False)
    class Meta:
        model = HomePageSliderCourrier
        fields = '__all__'


class HomePageSliderRideForm(forms.ModelForm):
    image = forms.ImageField(label='Image(380x684px)',required=False)
    class Meta:
        model = HomePageSliderRide
        fields = '__all__'


class HomeCMSForm(forms.ModelForm):
 
    banner = forms.ImageField(label='Banner Image(1920x650px)',required=False)
    banner_text = forms.CharField(label='Banner Heading', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    banner_desc = forms.CharField(label='Banner Description', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    
    # under_banner_title = forms.CharField(label='App download text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # under_banner_text = forms.CharField(label='App download text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    slider_title = forms.CharField(label='Title for Slider', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    slider_icon_courier = forms.ImageField(label='Slider Icon Ride (29X33px)', required=False)
    slider_icon_ride = forms.ImageField(label='Slider Icon Ride (29X33px)', required=False)

    service1_image = forms.ImageField(label='Image for service 1 (920x736px)', required=False)
    service1_title = forms.CharField(label='Title for service 1', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    service1_desc = forms.CharField(label='Description for service 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    service2_image = forms.ImageField(label='Image for service 2 (920x736px)', required=False)
    service2_title = forms.CharField(label='Title for service 2', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    service2_desc = forms.CharField(label='Description for service 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    features = forms.CharField(label='Features section headings', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    features_desc = forms.CharField(label='Features section description', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    feature1_image = forms.ImageField(label='Image for feature 1 (140X140px)', required=False)
    feature1_title = forms.CharField(label='Title for feature 1', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature1_desc = forms.CharField(label='Desctiption for feature 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature2_image = forms.ImageField(label='Image for feature 2 (140X140px)', required=False)
    feature2_title = forms.CharField(label='Title for feature 2', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature2_desc = forms.CharField(label='Desctiption for feature 2', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature3_image = forms.ImageField(label='Image for feature 3 (140X140px)', required=False)
    feature3_title = forms.CharField(label='Title for feature 3', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature3_desc = forms.CharField(label='Desctiption for feature 3', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    feature4_image = forms.ImageField(label='Image for feature 4 (140X140px)', required=False)
    feature4_title = forms.CharField(label='Title for feature 4', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature4_desc = forms.CharField(label='Desctiption for feature 4', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    patner_image_1 = forms.ImageField(label='Partner Logo (163X91px)', required=False)
    patner_image_2 = forms.ImageField(label='Partner Logo (163X91px)', required=False)
    patner_image_3 = forms.ImageField(label='Partner Logo (163X91px)', required=False)
    patner_image_4 = forms.ImageField(label='Partner Logo (163X91px)', required=False)
    patner_image_5 = forms.ImageField(label='Partner Logo (163X91px)', required=False)


    

    class Meta:
        model = HomeCMS
        fields =("banner", "banner_text", "banner_desc", 
            "slider_title", "slider_icon_courier", "slider_icon_ride",
            "features", "features_desc",
            "feature1_image", "feature1_title", "feature1_desc", "feature2_image", "feature2_title",
            "feature2_desc", "feature3_image", "feature3_title", "feature3_desc", "feature4_image",
            "feature4_title", "feature4_desc",
            "patner_image_1", "patner_image_2", "patner_image_3", "patner_image_4", "patner_image_5",
            "service1_title", "service1_desc", "service1_image", "service2_title", "service2_desc", "service2_image")

class AboutPageForm(forms.ModelForm):

    title1 = forms.CharField(label='Page Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc1 = forms.CharField(label='Page Description', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    image1 = forms.ImageField(label='Content Image 1 (882x646px)', required=False)
    image2 = forms.ImageField(label='Content Image 2 (374x250px)', required=False)
    image3 = forms.ImageField(label='Content Image 3 (374x250px)', required=False)
   

    comment = forms.CharField(label='Comment', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    comment_by = forms.CharField(label='Comment By', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    comment_text = forms.CharField(label='Comment Text', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    team_header = forms.CharField(label='Team Header', max_length=255, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    team1_image = forms.ImageField(label='Team Image 1 (300x300px)', required=False)
    team1_name = forms.CharField(label='Team Name 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    team1_desination = forms.CharField(label='Team Designation 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    team2_image = forms.ImageField(label='Team Image 2 (300x300px)', required=False)
    team2_name = forms.CharField(label='Team Name 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    team2_desination = forms.CharField(label='Team Designation 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    team3_image = forms.ImageField(label='Team Image 3 (300x300px)', required=False)
    team3_name = forms.CharField(label='Team Name 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    team3_desination = forms.CharField(label='Team Designation 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    
    link_text = forms.CharField(label='Link text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    link_desc = forms.CharField(label='Link Description', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    link = forms.CharField(label='Link', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    
    app_download_text = forms.CharField(label='App Download Text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    app_download_link = forms.CharField(label='App Download Link', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))



    class Meta:
        model = AboutPage
        fields = [
        'title1', 'desc1',
        'image1', 'image2', 'image3',
        'comment', 'comment_by', 'comment_text',
        'team_header',
        'team1_image', 'team1_name', 'team1_desination',
        'team2_image', 'team2_name', 'team2_desination',
        'team3_image', 'team3_name', 'team3_desination',
        'link_text', 'link_desc', 'link',
        'app_download_text', 'app_download_link'
        ]


class CustomerFaqPageForm(forms.ModelForm):
    question = forms.CharField(label='Question Title', max_length=120, required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    answer = forms.CharField(label='Description', required=True, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    class Meta:
        model = CustomerFaqPage
        fields = ['question',
        'answer']

class SliderImageForm(forms.ModelForm):
    title = forms.CharField(label='Slider Image Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc = forms.CharField(label='Slider Image Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    images = forms.ImageField(label="Image", required=False)

    class Meta:
        model = SliderImage
        fields = [
            'title',
            'desc',
            'images'
        ]

class RegistrationBackgroundForm(forms.ModelForm):
    title = forms.CharField(label='Background Image Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    images = forms.ImageField(label="Image", required=False)

    class Meta:
        model = RegistrationBackground
        fields = [
            'title',
            'images'
        ]

class ServicesPageForm(forms.ModelForm):
    # image1 = forms.ImageField(label='First Image (960x844)', required=False)

    title1 = forms.CharField(label='First Content Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc1 = forms.CharField(label='First Content Description', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    banner_image = forms.ImageField(label='Banner Image (1600x667px)', required=False)

    comment = forms.CharField(label='Comment', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    comment_by = forms.CharField(label='Comment By', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    comment_text = forms.CharField(label='Comment Text', max_length=120, required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards1_image = forms.ImageField(label='Rewards Image 1 (150x150px)', required=False)
    rewards1_text = forms.CharField(label='Rewards Text 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards1_desc = forms.CharField(label='Rewards Desc 1', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards2_image = forms.ImageField(label='Rewards Image 2 (150x150px)', required=False)
    rewards2_text = forms.CharField(label='Rewards Text 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards2_desc = forms.CharField(label='Rewards Desc 2', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards3_image = forms.ImageField(label='Rewards Image 3 (150x150px)', required=False)
    rewards3_text = forms.CharField(label='Rewards Text 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards3_desc = forms.CharField(label='Rewards Desc 3', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    rewards4_image = forms.ImageField(label='Rewards Image 4 (150x150px)', required=False)
    rewards4_text = forms.CharField(label='Rewards Text 4', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    rewards4_desc = forms.CharField(label='Rewards Desc 4', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    title2 = forms.CharField(label='Second Content Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc2 = forms.CharField(label='Second Content Description', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    
    promo_image1 = forms.ImageField(label='Promo Image 1 (150x150px)', required=False)
    promo_description1 = forms.CharField(label='Promo Description 1', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    promo_image2 = forms.ImageField(label='Promo Image 2 (150x150px)', required=False)
    promo_description2 = forms.CharField(label='Promo Description 2', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    
    promo_image3 = forms.ImageField(label='Promo Image 3 (150x150px)', required=False)
    promo_description3 = forms.CharField(label='Promo Description 3', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    
    buttom_image1 = forms.ImageField(label='Bottom Image 1 (756x510px)', required=False)
    buttom_image2 = forms.ImageField(label='Bottom Image 2 (374x250px)', required=False)
    buttom_image3 = forms.ImageField(label='Bottom Image 3 (374x250px)', required=False)

    app_download_text = forms.CharField(label='App Download Text', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    app_download_link = forms.CharField(label='App Download Link', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    buttom_title = forms.CharField(max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    title_for_same_day = forms.CharField(max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    description_for_same_day = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    title_for_same_nextday = forms.CharField(max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    description_for_nextday = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    class Meta:
        model = Services
        fields = [
        'title1', 'desc1',
        "banner_image",
        'comment', 'comment_by', 'comment_text',
        'rewards1_image', 'rewards1_text', 'rewards1_desc',
        'rewards2_image', 'rewards2_text', 'rewards2_desc',
        'rewards3_image', 'rewards3_text', 'rewards3_desc',
        'rewards4_image', 'rewards4_text', 'rewards4_desc',
        'title2', 'desc2',
        'promo_image1', 'promo_description1',
        'promo_image2', 'promo_description2',
        'promo_image3', 'promo_description3',
        'buttom_image1', 'buttom_image2', 'buttom_image3',
        'app_download_text', 'app_download_link',
        'buttom_title', 'title_for_same_day', 'description_for_same_day', 'title_for_same_nextday', 'description_for_nextday',
        ]


class CorporatePageForm(forms.ModelForm):

    title1 = forms.CharField(label='Corporate Title 1', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc1 = forms.CharField(label='Description 1', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    first_tab_title = forms.CharField(label='First Tab Title', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    first_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    first_tab_image = forms.ImageField(label='First Tab Image (1140x452px)', required=False)

    second_tab_title = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    second_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    second_tab_image = forms.ImageField(label='Second Tab Image (1140x452px)', required=False)

    thrid_tab_title = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    third_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    third_tab_image = forms.ImageField(label='Third Tab Image (1140x452px)', required=False)

    fourth_tab_title = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    fourth_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    fourth_tab_image = forms.ImageField(label='Fourth Tab Image (1140x452px)', required=False)

    title2 = forms.CharField(label='Corporate Title 2', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    first_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    second_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    third_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    fourth_tab_description = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    

    class Meta:
        model = Corporate
        fields = [
        'title1', 'desc1', 'title2',
        'first_tab_title', 'first_tab_description', 'first_tab_image',
        'second_tab_title', 'second_tab_description', 'second_tab_image',
        'thrid_tab_title', 'third_tab_description', 'third_tab_image',
        'fourth_tab_title', 'fourth_tab_description', 'fourth_tab_image',
        'bottom_button_text',
        ]

class JoinUsPageForm(forms.ModelForm):

    banner1 = forms.ImageField(label='First Banner Image (1500x672px)', required=False)
    title1 = forms.CharField(label='First Title', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    linke_text = forms.CharField(label='Button Link Text', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    linke = forms.CharField(label='Button Link', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    feature_header = forms.CharField(label='Feature Header', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    feature_image1 = forms.ImageField(label='Feature Image 1(150x150px)', required=False)
    feature_title1 = forms.CharField(label='Feature Title 1', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature_desc1 = forms.CharField(label='Feature Description 1', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    feature_image2 = forms.ImageField(label='Feature Image 2(150x150px)', required=False)
    feature_title2 = forms.CharField(label='Feature Title 2', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature_desc2 = forms.CharField(label='Feature Description 2', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    feature_image3 = forms.ImageField(label='Feature Image 3(150x150px)', required=False)
    feature_title3 = forms.CharField(label='Feature Title 3', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature_desc3 = forms.CharField(label='Feature Description 3', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    feature_image4 = forms.ImageField(label='Feature Image 4(150x150px)', required=False)
    feature_title4 = forms.CharField(label='Feature Title 4', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature_desc4 = forms.CharField(label='Feature Description 4', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    feature_image5 = forms.ImageField(label='Feature Image 5(150x150px)', required=False)
    feature_title5 = forms.CharField(label='Feature Title 5', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    feature_desc5 = forms.CharField(label='Feature Description 5', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    banner2 = forms.ImageField(label='Banner 2(1920x774px)', required=False)
    title2 = forms.CharField(label='Second Title', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    title3 = forms.CharField(label='Last Title', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc3 = forms.CharField(label='Last Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))


    class Meta:
        model = JoinUs
        fields = [
        'banner1', 'title1',
        'feature_header',
        'feature_image1', 'feature_title1', 'feature_desc1',
        'feature_image2', 'feature_title2', 'feature_desc2',
        'feature_image3', 'feature_title3', 'feature_desc3',
        'feature_image4', 'feature_title4', 'feature_desc4',
        'feature_image5', 'feature_title5', 'feature_desc5',
        'banner2', 'title2',
        'title3', 'desc3',
        'linke_text', 'linke'
        ]


class ContactUsPageForm(forms.ModelForm):
    title = forms.CharField(label='Title', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    desc = forms.CharField(label='Description', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    location_image1 = forms.ImageField(label='Location Image(555x193px)', required=False)
    location_title1 = forms.CharField(label='Location', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    location_desc1 = forms.CharField(label='City', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    # location_image2 = forms.ImageField(label='Location Image 2', required=False)
    # location_title2 = forms.CharField(label='Location 2', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # location_desc2 = forms.CharField(label='City 2', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    # location_image3 = forms.ImageField(label='Location Image 3', required=False)
    # location_title3 = forms.CharField(label='Location 3', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # location_desc3 = forms.CharField(label='City 3', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    bottom_description = forms.CharField(label='Bottom Title', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = ContactUsPage
        fields = (
        'title', 'desc',
        'location_image1', 'location_title1', 'location_desc1',
        # 'location_image2', 'location_title2', 'location_desc2',
        # 'location_image3', 'location_title3', 'location_desc3',
        'bottom_description'
        )

class TermsForm(forms.ModelForm):
    title = forms.CharField(label='Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Title'}))
    description = forms.CharField(label='Description', required=False,
                           widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    class Meta:
        model = Terms
        fields = [
            'title', 'description',
        ]


class FaqForm(forms.ModelForm):
    title = forms.CharField(label='Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Title'}))
    description = forms.CharField(label='Description', required=False,
                           widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    class Meta:
        model = Faq
        fields = [
            'title', 'description',
        ]

class PrivacyForm(forms.ModelForm):
    title = forms.CharField(label='Title', max_length=120, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control huge', 'placeholder': 'Title'}))
    description = forms.CharField(label='Description', required=False,
                           widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    class Meta:
        model = Privacy
        fields = [
            'title', 'description',
        ]


class CareersPageForm(forms.ModelForm):
    #banner image in career
    banner = forms.ImageField(label='Career banner image', required=False)
    #title of the page
    title = forms.CharField(label='Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    #description right beneath the title
    desc = forms.CharField(label='Description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    #for each job, there will be title, description and list of responsibility
    job_title = forms.CharField(label='Job Title', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    job_desc = forms.CharField(label='Job description', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))
    job_responsibility = forms.CharField(label='Job responsibility', required=False, widget=forms.Textarea(attrs={'class': 'form-control huge'}))

    class Meta:
        model = Careers
        fields = [
        'banner',
        'title', 'desc',
        'job_title', 'job_desc', 'job_responsibility',
        ]
        



#PACKAGE SETUP FORM START

class PackageForm(forms.ModelForm):
    name = forms.CharField(label='Package Name', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge', 'required':'required'}))
    # weight = forms.CharField(label='Weight', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    length = forms.CharField(label='Length', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    width = forms.CharField(label='Width', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    height = forms.CharField(label='Height', required=False, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    start_weight = forms.CharField(label='Start Weight', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    end_weight = forms.CharField(label='End Weight', max_length=120, required=False, widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))

    class Meta:
        model = PackageSize
        fields = [
        'name', 'start_weight', 'end_weight', 'length', 'width', 'height'
        ]

#PACKAGE SETUP FORM END

class pricingForm(forms.ModelForm):
    schedule_type = forms.ChoiceField(label='Schedule Type', choices=SCHEDULE_TYPE, widget=forms.Select(attrs={'class': 'form-control huge'}))
    package_size = forms.ModelChoiceField(label='Package Size', queryset=PackageSize.objects.all(), widget=forms.Select(attrs={'class': 'form-control huge'}))
    start_qty = forms.IntegerField(label='Start Quantity', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    end_qty = forms.IntegerField(label='End Quantity', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    range_rate = forms.DecimalField(label='Standard Rate', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    corp_range_rate = forms.DecimalField(label='Corporate rate', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    
    class Meta:
        model = Pricing
        fields = ['schedule_type', 'package_size', 'start_qty', 'end_qty',
                    'range_rate', 'corp_range_rate',
                 ]

#VEHICLE SETUP FORM START

class VehicleForm(forms.ModelForm):
    caption = forms.CharField(label="Vehicle Name", max_length=120, required=True, widget=forms.TextInput(attrs={'class':'form-control huge'}))
    description = forms.CharField(label="description", max_length=120, required=False, widget=forms.TextInput(attrs={'class':'form-control huge'}))
    pic = forms.ImageField(label="Upload a picture", required=True)
    pic_active = forms.ImageField(label="Upload a picture for Active Status", required=True)
    pic_detail = forms.ImageField(label="Upload a picture for Detail", required=True)

    class Meta:
        model = VehicleType
        fields = [
        'caption','description', 'pic', 'pic_active', 'pic_detail', 'service_type'
        ]
#VEHICLE SETUP FORM END


#HOLIDAY SETUP FORM START

class HolidayForm(forms.ModelForm):
    date = forms.DateField(label='date', required=False, widget=forms.DateInput(attrs={'class':'form-control input-lg','data-plugin': 'bs-date', 'placeholder': 'Date', 'id':'date', 'required':'required'}))
    caption = forms.CharField(label='caption', max_length=500, required=False, widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'placeholder': 'Description', 'required':'required'}))
    # input_formats = '%d-%c-%Y',
    class Meta:
        model = HolidayList
        fields = [
        'date', 'caption',
        ]

#HOLIDAY SETUP FORM END


#POSTALURCHARGE SETUP FORM START

class SurchargeForm(forms.ModelForm):
    postal_code = forms.CharField(label='Postal Code', required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    charge = forms.DecimalField(label='Fee', required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # corp_charge = forms.DecimalField(label='Fee', required=True,
    #                                  widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    description = forms.CharField(label='Description', required=True, widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = PickupSurgeCharge
        fields = [
        'postal_code', 'charge',  'description'
        ]

class CourierSurgeChargeForm(forms.ModelForm):
    postal_code = forms.CharField(label='Postal Code', required=True,
                                  widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    charge = forms.DecimalField(label='Fee', required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    # corp_charge = forms.DecimalField(label='Fee', required=True,
    #                             widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    description = forms.CharField(label='Description', required=True,
                                  widget=forms.TextInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = CourierSurgeCharge
        fields = [
            'postal_code', 'charge', 'description'
        ]
#POSTALSURCHARGE SETUP FORM END


#WEEKDAYPICKUPRATES SETUP FORM START

class PickupRateForm(forms.ModelForm):
    # query = VehicleType.objects.all()
    vehicle_type = forms.ModelChoiceField(label='Vehicle Type', queryset=VehicleType.objects.all(), widget=forms.Select(attrs={'class': 'form-control huge' ,'required':'required'}))
    min_fare = forms.DecimalField(label='Min Fare', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    base_fare = forms.DecimalField(label='Base Fare', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    rate = forms.DecimalField(label='Rate', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    waiting_charge = forms.DecimalField(label='Hour\'s charge', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    peakMultiplier = forms.DecimalField(label='Sur Charge', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    gst = forms.DecimalField(label='GST %', widget=forms.NumberInput(attrs={'class': 'form-control huge','required': 'required'}))
    day_type = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))

    class Meta:
        model = PickUpRate
        fields = [
        'vehicle_type', 'min_fare', 'base_fare', 'rate', 'waiting_charge', 'peakMultiplier'
        , 'gst', 'day_type',
        ]

#WEEKDAYPICKUPRATES SETUP FORM END
class DriverStatusForm(forms.ModelForm):
    driver_type = forms.CharField(label='Driver Type', widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    courier_commission = forms.DecimalField(label='Courier Commission', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    pickup_commission = forms.DecimalField(label='Pickup Commission' ,widget=forms.NumberInput(attrs={'class': 'form-control huge'}))

    class Meta:
        model = DriverStatus
        fields = [
            'driver_type', 'courier_commission', 'pickup_commission',
        ]

class ExpressWindowsForm(forms.ModelForm):
    """
        Now this from is for delivery window only
    """
    window_name = forms.CharField(label='Window Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    # start_time = forms.TimeField(required=False, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
    #                                                            'id': 'start-time'}))
    # end_time = forms.TimeField(required=False, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
    #                                                            'id': 'start-time'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    # corp_price = forms.DecimalField(label='Corporate Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    max_number_of_active_job = forms.IntegerField(label='Max Number of Active Job',  widget=forms.NumberInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    class Meta:
        model = ExpressWindows
        fields = [
            'window_name',
            # 'start_time',
            # 'end_time',
            'price',
            # 'corp_price',
            'multiple',
            'max_number_of_active_job'
        ]

class PickupGSTForm(forms.ModelForm):
    """
        Now this from is for Pickup GST only
    """
    # name = forms.CharField(label='Window Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    class Meta:
        model = PickupGST
        fields = [
            # 'name',
            'percentage',
            'price'
        ]


class CourierGSTForm(forms.ModelForm):
    """
        Now this from is for Pickup GST only
    """
    # name = forms.CharField(label='Window Name', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    price = forms.DecimalField(label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'value': '0.00'}))
    class Meta:
        model = CourierGST
        fields = [
            # 'name',
            'percentage',
            'price'
        ]

class PickupTimeChargeForm(forms.ModelForm):
    """
        Now this from is for delivery window only
    """
    postal_code = forms.CharField(required=True, label='Postal Code', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick','id': 'start-time', 'required': 'required'}))
    end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time', 'required': 'required'}))
    price = forms.DecimalField(required=True, label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    class Meta:
        model = PickupTimeCharge
        fields = [
            'postal_code',
            'start_time',
            'end_time',
            'price'
        ]

class PickupTimeChargeFormBulk(forms.ModelForm):
    """
        Now this from is for delivery window only
    """
    postal_code = forms.CharField(label='Postal Code', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick','id': 'start-time'}))
    end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time'}))
    price = forms.DecimalField(required=True, label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge'}))
    class Meta:
        model = PickupTimeCharge
        fields = [
            'postal_code',
            'start_time',
            'end_time',
            'price'
        ]
class CourierTimeChargeForm(forms.ModelForm):
    """
        Now this from is for delivery window only
    """
    postal_code = forms.CharField(label='Postal Code', widget=forms.TextInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    start_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time', 'required': 'required'}))
    end_time = forms.TimeField(required=True, input_formats=['%I:%M %p'], widget=forms.TimeInput(format='%I:%M %p', attrs={'class': 'form-control huge time-pick',
                                                               'id': 'start-time', 'required': 'required'}))
    price = forms.DecimalField(required=True, label='Price', widget=forms.NumberInput(attrs={'class': 'form-control huge', 'required': 'required'}))
    class Meta:
        model = CourierTimeCharge
        fields = [
            'postal_code',
            'start_time',
            'end_time',
            'price'
        ]

class CommissionSetupForm(forms.ModelForm):
    category = forms.CharField(label='Category', widget=forms.Select( choices=constant.DRIVER_TYPE,
        attrs={'class': 'form-control huge', 'required': 'required'}))
    commission_courier = forms.DecimalField(label='Courier Commission',
                                            widget=forms.NumberInput(attrs={'class': 'form-control huge',
                                                                            'required': 'required'}))
    commission_pickup = forms.DecimalField(label='Pickup Commission',
                                           widget=forms.NumberInput(attrs={'class': 'form-control huge',
                                                                           'required': 'required'}))
    class Meta:
        model = CommissionSetup
        fields = [
            'category', 'commission_pickup', 'commission_courier'
        ]
class SiteSettingsForm(forms.ModelForm):
    max_minuts_for_cencellations = forms.CharField(label='Max Cancellation Time (Min)', max_length=120, required=True,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    driver_radius = forms.CharField(label='Driver Radius (Km)', max_length=120, required=True,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    waiting_time_for_drivers = forms.CharField(label='Waiting Time for drivers(second)', max_length=120, required=True,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    cancellatin_charge = forms.CharField(label='Cancellation charge ($)', max_length=120, required=True,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    class Meta:
        model = SiteConfigeration
        fields = ['max_minuts_for_cencellations', 'driver_radius', 'waiting_time_for_drivers', 'cancellatin_charge']


class PickupCancellationReasonsForm(forms.ModelForm):
    reason = forms.CharField(label='Reason', max_length=120, required=True,
                             widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image = forms.ImageField(label='Icon', required=True)
    image_active = forms.ImageField(label='Icon Active', required=True)
    class Meta:
        model = PickupCancellationReasons
        fields = ['image', 'image_active', 'reason']

class CourierCancellationReasonsForm(forms.ModelForm):
    reason = forms.CharField(label='Reason', max_length=120, required=True,
                             widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    image = forms.ImageField(label='Icon', required=True)
    image_active = forms.ImageField(label='Icon Active', required=True)
    class Meta:
        model = CourierCancellationReasons
        fields = ['image', 'image_active', 'reason']

class RotatingBannerForm(forms.ModelForm):
    image = forms.ImageField(label='Icon', required=True)
    title = forms.CharField(label='Title', max_length=120, required=True,
                            widget=forms.TextInput(attrs={'class': 'form-control huge'}))
    class Meta:
        model=RotatingBanner
        fields = ['image', 'title']

class StringSetupForm(forms.ModelForm):
    class Meta:
        model = StringSetup
        fields = ['disclaimer_on_package_guide']