from django.db import models
from base import constant
from django.contrib.auth.models import User


# Create your models here.

class HomePageSliderCourrier(models.Model):
    image = models.ImageField(blank=True, null=True, upload_to='slider/courier')
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)

class HomePageSliderRide(models.Model):
    image = models.ImageField(blank=True, null=True, upload_to='slider/courier')
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)


class CommonCms(models.Model):
    logo = models.ImageField(blank=True, null=True, upload_to='common')
    logo_white_color = models.ImageField(blank=True, null=True, upload_to='common')
    facebook_image = models.ImageField(blank=True, null=True, upload_to='common')
    facebook_link = models.CharField(max_length=500, blank=True, null=True)
    twitter_image = models.ImageField(blank=True, null=True, upload_to='common')
    twitter_link = models.CharField(max_length=500, blank=True, null=True)
    instagram_image = models.ImageField(blank=True, null=True, upload_to='common')
    instagram_link = models.CharField(max_length=500, blank=True, null=True)
    youtube_image = models.ImageField(blank=True, null=True, upload_to='common')
    youtube_link = models.CharField(max_length=500, blank=True, null=True)
    android_app_download_link = models.CharField(max_length=255, blank=True, null=True)
    iso_app_download_link = models.CharField(max_length=255, blank=True, null=True)
    copyright_text = models.CharField(max_length=500, blank=True, null=True)

    company_address = models.TextField(blank=True, null=True)
    company_address_latitude = models.CharField(max_length=255, blank=True, null=True)
    company_address_longitude = models.CharField(max_length=255, blank=True, null=True)
    company_phone_no = models.CharField(max_length=255, blank=True, null=True)
    company_fax_no = models.CharField(max_length=255, blank=True, null=True)
    company_email = models.CharField(max_length=255, blank=True, null=True)
    company_website = models.CharField(max_length=255, blank=True, null=True)

    dashboard_payment_page_header_text = models.CharField(max_length=500, blank=True, null=True)
    dashboard_payment_page_header_image = models.ImageField(blank=True, null=True, upload_to='common')

class HomeCMS(models.Model):
    
    banner = models.ImageField(blank=True, null=True, upload_to='home')
    banner_text = models.CharField(max_length=255, blank=True, null=True)
    banner_desc = models.CharField(max_length=255, blank=True, null=True)

    under_banner_title = models.CharField(max_length=255, blank=True, null=True)
    under_banner_text = models.CharField(max_length=255, blank=True, null=True)

    slider_title = models.CharField(max_length=500, blank=True, null=True)
    slider_icon_courier = models.ImageField(blank=True, null=True, upload_to='home')
    slider_icon_ride = models.ImageField(blank=True, null=True, upload_to='home')

    service1_image = models.ImageField(blank=True, null=True, upload_to='home')
    service1_title = models.CharField(max_length=255, blank=True, null=True)
    service1_desc = models.TextField(blank=True, null=True)

    service2_image = models.ImageField(blank=True, null=True, upload_to='home')
    service2_title = models.CharField(max_length=255, blank=True, null=True)
    service2_desc = models.TextField(blank=True, null=True)

    features = models.CharField(max_length=255, blank=True, null=True)
    features_desc = models.CharField(max_length=255, blank=True, null=True)

    feature1_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature1_title = models.CharField(max_length=255, blank=True, null=True)
    feature1_desc = models.TextField(blank=True, null=True)

    feature2_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature2_title = models.CharField(max_length=255, blank=True, null=True)
    feature2_desc = models.TextField(blank=True, null=True)

    feature3_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature3_title = models.CharField(max_length=255, blank=True, null=True)
    feature3_desc = models.TextField(blank=True, null=True)

    feature4_image = models.ImageField(blank=True, null=True, upload_to='home/feature')
    feature4_title = models.CharField(max_length=255, blank=True, null=True)
    feature4_desc = models.TextField(blank=True, null=True)

    patner_image_1 = models.ImageField(blank=True, null=True, upload_to='home/patner')
    patner_image_2 = models.ImageField(blank=True, null=True, upload_to='home/patner')
    patner_image_3 = models.ImageField(blank=True, null=True, upload_to='home/patner')
    patner_image_4 = models.ImageField(blank=True, null=True, upload_to='home/patner')
    patner_image_5 = models.ImageField(blank=True, null=True, upload_to='home/patner')

    def __unicode__(self):
        return "Index page edits"

class Terms(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

class Faq(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

class Privacy(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

class AboutPage(models.Model):

    title1 = models.CharField(max_length=120, blank=True, null=True)
    desc1 = models.TextField(blank=True, null=True)

    image1 = models.ImageField(blank=True, null=True, upload_to='about')
    image2 = models.ImageField(blank=True, null=True, upload_to='about')
    image3 = models.ImageField(blank=True, null=True, upload_to='about')
    

    comment = models.CharField(max_length=500, blank=True, null=True)
    comment_by = models.CharField(max_length=500, blank=True, null=True)
    comment_text = models.TextField(blank=True, null=True)

    team_header = models.CharField(max_length=255, blank=True, null=True)

    team1_image = models.ImageField(blank=True, null=True, upload_to='about')
    team1_name = models.CharField(max_length=120, blank=True, null=True)
    team1_desination = models.CharField(max_length=120, blank=True, null=True)

    team2_image = models.ImageField(blank=True, null=True, upload_to='about')
    team2_name = models.CharField(max_length=120, blank=True, null=True)
    team2_desination = models.CharField(max_length=120, blank=True, null=True)

    team3_image = models.ImageField(blank=True, null=True, upload_to='about')
    team3_name = models.CharField(max_length=120, blank=True, null=True)
    team3_desination = models.CharField(max_length=120, blank=True, null=True)


    link_text = models.CharField(max_length=120, blank=True, null=True)
    link_desc = models.CharField(max_length=120, blank=True, null=True)
    link = models.CharField(max_length=120, blank=True, null=True)

    app_download_text = models.CharField(max_length=120, blank=True, null=True)
    app_download_link = models.CharField(max_length=120, blank=True, null=True)

class CustomerFaqPage(models.Model):
    # banner = models.ImageField(blank=True, null=True, upload_to='about/banner')

    question = models.CharField(max_length=120, blank=True, null=True)
    answer = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "Customer Faq"

class SliderImage(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    desc = models.TextField(blank=True, null=True)
    images = models.ImageField(blank=True, null=True, upload_to='about/iteam')

    def __unicode__(self):
        return "About page edits"

class RegistrationBackground(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    images = models.ImageField(blank=True, null=True, upload_to='login/images')

    def __unicode__(self):
        return "About page edits"

class Services(models.Model):
    title1 = models.CharField(max_length=500, blank=True, null=True)
    desc1 = models.TextField(blank=True, null=True)

    banner_image = models.ImageField(blank=True, null=True, upload_to='services/banner')

    comment = models.CharField(max_length=120, blank=True, null=True)
    comment_by = models.CharField(max_length=120, blank=True, null=True)
    comment_text = models.TextField(blank=True, null=True)

    reword_title = models.CharField(max_length=120, blank=True, null=True)

    rewards1_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards1_text = models.CharField(max_length=120, blank=True, null=True)
    rewards1_desc = models.TextField(blank=True, null=True)

    rewards2_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards2_text = models.CharField(max_length=120, blank=True, null=True)
    rewards2_desc = models.TextField(blank=True, null=True)

    rewards3_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards3_text = models.CharField(max_length=120, blank=True, null=True)
    rewards3_desc = models.TextField(blank=True, null=True)

    rewards4_image = models.ImageField(blank=True, null=True, upload_to='services/reward')
    rewards4_text = models.CharField(max_length=120, blank=True, null=True)
    rewards4_desc = models.TextField(blank=True, null=True)

    title2 = models.CharField(max_length=120, blank=True, null=True)
    desc2 = models.CharField(max_length=120, blank=True, null=True)

    promo_image1 = models.ImageField(blank=True, null=True, upload_to='services/promo')
    promo_description1 = models.CharField(max_length=120, blank=True, null=True)

    promo_image2 = models.ImageField(blank=True, null=True, upload_to='services/promo')
    promo_description2 = models.CharField(max_length=120, blank=True, null=True)

    promo_image3 = models.ImageField(blank=True, null=True, upload_to='services/promo')
    promo_description3 = models.CharField(max_length=120, blank=True, null=True)

    buttom_image1 = models.ImageField(blank=True, null=True, upload_to='services/buttom')
    buttom_image2 = models.ImageField(blank=True, null=True, upload_to='services/buttom')
    buttom_image3 = models.ImageField(blank=True, null=True, upload_to='services/buttom')

    buttom_title = models.CharField(max_length=500, blank=True, null=True)
    title_for_same_day = models.CharField(max_length=500, blank=True, null=True)
    description_for_same_day = models.TextField(blank=True, null=True)
    title_for_same_nextday = models.CharField(max_length=500, blank=True, null=True)
    description_for_nextday = models.TextField(blank=True, null=True)

    app_download_text = models.CharField(max_length=120, blank=True, null=True)
    app_download_link = models.CharField(max_length=120, blank=True, null=True)

    def __unicode__(self):
        return "Our Process"


class JoinUs(models.Model):

    banner1 = models.ImageField(blank=True, null=True, upload_to='joinus/banner')
    title1 = models.CharField(max_length=500, blank=True, null=True)
    
    linke_text = models.CharField(max_length=500, blank=True, null=True)
    linke = models.CharField(max_length=500, blank=True, null=True)

    feature_header = models.CharField(max_length=500, blank=True, null=True)

    feature_image1 = models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature_title1 = models.CharField(max_length=500, blank=True, null=True)
    feature_desc1 = models.TextField(blank=True, null=True)

    feature_image2 = models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature_title2 = models.CharField(max_length=500, blank=True, null=True)
    feature_desc2 = models.TextField(blank=True, null=True)

    feature_image3 = models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature_title3 = models.CharField(max_length=500, blank=True, null=True)
    feature_desc3 = models.TextField(blank=True, null=True)

    feature_image4 = models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature_title4 = models.CharField(max_length=500, blank=True, null=True)
    feature_desc4 = models.TextField(blank=True, null=True)

    feature_image5 = models.ImageField(blank=True, null=True, upload_to='joinus/feature')
    feature_title5 = models.CharField(max_length=500, blank=True, null=True)
    feature_desc5 = models.TextField(blank=True, null=True)


    banner2 = models.ImageField(blank=True, null=True, upload_to='joinus/banner')
    title2 = models.CharField(max_length=500, blank=True, null=True)


    title3 = models.CharField(max_length=500, blank=True, null=True)
    desc3 = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "Join Us"

class ContactUsPage(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    desc = models.TextField(blank=True, null=True)

    location_image1 = models.ImageField(blank=True, null=True, upload_to='contactus/location')
    location_title1 = models.CharField(max_length=255, blank=True, null=True)
    location_desc1 = models.CharField(max_length=255 , blank=True, null=True)

    location_image2 = models.ImageField(blank=True, null=True, upload_to='contactus/location')
    location_title2 = models.CharField(max_length=255, blank=True, null=True)
    location_desc2 = models.CharField(max_length=255,blank=True, null=True)

    location_image3 = models.ImageField(blank=True, null=True, upload_to='contactus/location')
    location_title3 = models.CharField(max_length=255, blank=True, null=True)
    location_desc3 = models.CharField(max_length=255, blank=True, null=True)

    bottom_description = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return "ContactUs"


class Careers(models.Model):

    banner = models.ImageField(blank=True, null=True, upload_to='career')

    title = models.CharField(max_length=120, blank=True, null=True)
    desc = models.TextField(blank=True, null=True)

    job_title = models.CharField(max_length=120, blank=True, null=True)
    job_desc = models.TextField(blank=True, null=True)
    job_responsibility = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "Careers"

class Corporate(models.Model):

    title1 = models.CharField(max_length=500, blank=True, null=True)
    desc1 = models.CharField(max_length=500, blank=True, null=True)

    title2 = models.CharField(max_length=500, blank=True, null=True)

    first_tab_title = models.CharField(max_length=500, blank=True, null=True)
    first_tab_description = models.TextField(blank=True, null=True)
    first_tab_image = models.ImageField(blank=True, null=True, upload_to='corporate')

    second_tab_title = models.CharField(max_length=500, blank=True, null=True)
    second_tab_description = models.TextField(blank=True, null=True)
    second_tab_image = models.ImageField(blank=True, null=True, upload_to='corporate')

    thrid_tab_title = models.CharField(max_length=500, blank=True, null=True)
    third_tab_description = models.TextField(blank=True, null=True)
    third_tab_image = models.ImageField(blank=True, null=True, upload_to='corporate')

    fourth_tab_title = models.CharField(max_length=500, blank=True, null=True)
    fourth_tab_description = models.TextField(blank=True, null=True)
    fourth_tab_image = models.ImageField(blank=True, null=True, upload_to='corporate')
    
    bottom_button_text = models.CharField(max_length=500, blank=True, null=True)
    # bottom_button_link = models.CharField(max_length=500, blank=True, null=True)

    def __unicode__(self):
        return "Corporate"


class DriverStatus(models.Model):
    driver_type = models.CharField(max_length=120)
    courier_commission = models.DecimalField(max_digits=5, decimal_places=2)
    pickup_commission = models.DecimalField(max_digits=5, decimal_places=2)
    def __unicode__(self):
        return self.driver_type

class DeliveryTiming(models.Model):
    """
        Now this model is for collection window only
    """
    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)
    window_name = models.CharField(max_length=120, null=True)
    max_number_of_active_job = models.IntegerField(default=1000)

class DriverCategory(models.Model):
    category_name = models.CharField(max_length=120, null=True)
    category_details = models.CharField(max_length=120, null=True)
    def __unicode__(self):
        return self.category_name

class ExpressWindows(models.Model):
    """
        Now this model is for delivery window only
    """
    window_name = models.CharField(max_length=120, null=True)
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)
    corp_price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)
    multiple = models.BooleanField(default=False)
    max_number_of_active_job = models.IntegerField(default=1000)

    def __unicode__(self):
        return self.window_name


class CommissionSetup(models.Model):
    category = models.CharField(max_length=100, choices=constant.DRIVER_TYPE, default='NORMAL', null=True)
    commission_pickup = models.DecimalField(max_digits=5, decimal_places=2)
    commission_courier = models.DecimalField(max_digits=5, decimal_places=2)

class SiteConfigeration(models.Model):
    max_delivery_ojb = models.IntegerField()
    max_minuts_for_cencellations = models.IntegerField(default=100)
    office_address = models.TextField(blank=True, null=True)
    office_phone_no = models.CharField(max_length=120,blank=True, null=True)
    office_email = models.CharField(max_length=120,blank=True, null=True)
    driver_radius = models.IntegerField(default=5000)
    waiting_time_for_drivers = models.IntegerField(default=15)
    cancellatin_charge = models.DecimalField(max_digits=7, decimal_places=2, default=6.00)
    # job_expire_days_courier = models.IntegerField(default=15)

class AccessControl(models.Model):
    user = models.ForeignKey(User, null=True)
    is_cms = models.BooleanField(default=False)
    is_settings = models.BooleanField(default=False)
    is_courier = models.BooleanField(default=False)
    is_pickup = models.BooleanField(default=False)
    is_driver_info = models.BooleanField(default=False)
    is_sales_report = models.BooleanField(default=False)
    is_customer_list = models.BooleanField(default=False)
    is_account = models.BooleanField(default=False)
    is_add_coupon = models.BooleanField(default=False)
    is_window_setup = models.BooleanField(default=False)
    is_address_setup = models.BooleanField(default=False)

class RotatingBanner(models.Model):
    image = models.ImageField(blank=True, null=True, upload_to='home/banner')
    title = models.CharField(max_length=120, blank=True, null=True)


class StringSetup(models.Model):
    registration_error_text = models.CharField(max_length=255, null=True)
    login_error_text = models.CharField(max_length=255, null=True)
    no_driver_found_text = models.CharField(max_length=255, null=True)
    pickup_job_taken_text = models.CharField(max_length=255, null=True)
    pickup_job_cancelled_text = models.CharField(max_length=255, null=True)
    new_courier_job_post = models.CharField(max_length=255, null=True)
    credit_terms_error = models.CharField(max_length=500,blank=True, null=True)
    disclaimer_on_package_guide = models.CharField(max_length=500,blank=True, null=True)

class PushMessage(models.Model):
    new_courier_job = models.CharField(max_length=500,blank=True, null=True)
    courier_job_cancel_by_driver = models.CharField(max_length=500,blank=True, null=True)
    courier_job_cancel_by_customer = models.CharField(max_length=500,blank=True, null=True)
    courier_job_cancel_by_system = models.CharField(max_length=500,blank=True, null=True)
    confirm_customer_ride_taken = models.CharField(max_length=500,blank=True, null=True)
    confirm_customer_ride_completed = models.CharField(max_length=500,blank=True, null=True)
    ride_cancel_by_driver = models.CharField(max_length=500,blank=True, null=True)
    ride_cancel_by_customer = models.CharField(max_length=500,blank=True, null=True)
    carged_for_ride_job = models.CharField(max_length=500,blank=True, null=True)
    carged_for_ride_cancellation = models.CharField(max_length=500,blank=True, null=True)
    other_device_login_for_consumer = models.CharField(max_length=500,blank=True, null=True)
    other_device_login_for_driver = models.CharField(max_length=500,blank=True, null=True)
    job_assigned_for_driver = models.CharField(max_length=500,blank=True, null=True)
    driver_arrived = models.CharField(max_length=500,blank=True, null=True)