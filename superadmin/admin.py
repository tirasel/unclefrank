from django.contrib import admin
from service.models import CartonType, ScheduleType, DayType, Pricing

# Register your models here.
# admin.site.register(index_page_text_pictures)
# admin.site.register(AboutPage)

admin.site.register(CartonType)
admin.site.register(ScheduleType)
admin.site.register(DayType)
admin.site.register(Pricing)
