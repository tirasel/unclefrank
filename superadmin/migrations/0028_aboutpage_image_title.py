# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0027_remove_contactuspage_addr_addr'),
    ]

    operations = [
        migrations.AddField(
            model_name='aboutpage',
            name='image_title',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
