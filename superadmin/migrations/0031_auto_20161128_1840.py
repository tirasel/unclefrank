# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0030_commissionsetup'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commissionsetup',
            name='category',
            field=models.CharField(default=b'NORMAL', max_length=100, null=True, choices=[(b'NORMAL', b'NORMAL'), (b'BRONZE', b'BRONZE'), (b'SILVER', b'SILVER'), (b'GOLD', b'GOLD')]),
        ),
    ]
