# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0057_services_banner_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='corporate',
            name='banner',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='desc2',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='desc3',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='desc4',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='image1',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='image2',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='image3',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='image4',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='link1_text',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='link2_text',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='link3_text',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='link4_text',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='title3',
        ),
        migrations.RemoveField(
            model_name='corporate',
            name='title4',
        ),
        migrations.AlterField(
            model_name='corporate',
            name='desc1',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
