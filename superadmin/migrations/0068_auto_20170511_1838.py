# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0067_auto_20170511_1505'),
    ]

    operations = [
        migrations.AddField(
            model_name='homecms',
            name='slider_icon_courier',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AddField(
            model_name='homecms',
            name='slider_icon_ride',
            field=models.ImageField(null=True, upload_to=b'home', blank=True),
        ),
        migrations.AddField(
            model_name='homecms',
            name='slider_title',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
