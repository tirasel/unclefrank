# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0084_auto_20170713_1341'),
    ]

    operations = [
        migrations.AddField(
            model_name='pushmessage',
            name='job_assigned_for_driver',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
