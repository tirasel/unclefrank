# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0077_joinus_feature_header'),
    ]

    operations = [
        migrations.AddField(
            model_name='joinus',
            name='linke_text',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
