# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0070_auto_20170511_2004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commoncms',
            name='copyright_text',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commoncms',
            name='facebook_link',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commoncms',
            name='instagram_link',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commoncms',
            name='twitter_link',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commoncms',
            name='youtube_link',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
