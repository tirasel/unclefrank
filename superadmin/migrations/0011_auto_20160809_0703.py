# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0010_careers_contactuspage_corporate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='services',
            name='desc1',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='desc2',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='desc3',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='desc4',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='rewards1_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='rewards2_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='rewards3_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='rewards4_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='small_icon_desc',
            field=models.TextField(null=True, blank=True),
        ),
    ]
