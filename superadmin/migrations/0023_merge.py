# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0022_terms'),
        ('superadmin', '0022_drivercategory'),
    ]

    operations = [
    ]
