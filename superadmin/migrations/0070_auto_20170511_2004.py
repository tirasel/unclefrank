# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0069_auto_20170511_1907'),
    ]

    operations = [
        migrations.AddField(
            model_name='corporate',
            name='bottom_button_text',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='first_tab_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='first_tab_image',
            field=models.ImageField(null=True, upload_to=b'corporate', blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='first_tab_title',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='fourth_tab_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='fourth_tab_image',
            field=models.ImageField(null=True, upload_to=b'corporate', blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='fourth_tab_title',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='second_tab_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='second_tab_image',
            field=models.ImageField(null=True, upload_to=b'corporate', blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='second_tab_title',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='third_tab_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='third_tab_image',
            field=models.ImageField(null=True, upload_to=b'corporate', blank=True),
        ),
        migrations.AddField(
            model_name='corporate',
            name='thrid_tab_title',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='corporate',
            name='desc1',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='corporate',
            name='title1',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='corporate',
            name='title2',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
