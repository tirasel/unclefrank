# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0055_auto_20170412_1918'),
    ]

    operations = [
        migrations.RenameField(
            model_name='services',
            old_name='tab1',
            new_name='app_download_link',
        ),
        migrations.RemoveField(
            model_name='services',
            name='desc3',
        ),
        migrations.RemoveField(
            model_name='services',
            name='desc4',
        ),
        migrations.RemoveField(
            model_name='services',
            name='image1',
        ),
        migrations.RemoveField(
            model_name='services',
            name='image2',
        ),
        migrations.RemoveField(
            model_name='services',
            name='image3',
        ),
        migrations.RemoveField(
            model_name='services',
            name='slider_image1',
        ),
        migrations.RemoveField(
            model_name='services',
            name='slider_image2',
        ),
        migrations.RemoveField(
            model_name='services',
            name='slider_image3',
        ),
        migrations.RemoveField(
            model_name='services',
            name='slider_image4',
        ),
        migrations.RemoveField(
            model_name='services',
            name='slider_image5',
        ),
        migrations.RemoveField(
            model_name='services',
            name='small_icon',
        ),
        migrations.RemoveField(
            model_name='services',
            name='small_icon_desc',
        ),
        migrations.RemoveField(
            model_name='services',
            name='tab1_desc',
        ),
        migrations.RemoveField(
            model_name='services',
            name='tab2',
        ),
        migrations.RemoveField(
            model_name='services',
            name='tab2_desc',
        ),
        migrations.RemoveField(
            model_name='services',
            name='tab3',
        ),
        migrations.RemoveField(
            model_name='services',
            name='tab3_desc',
        ),
        migrations.RemoveField(
            model_name='services',
            name='title3',
        ),
        migrations.RemoveField(
            model_name='services',
            name='title4',
        ),
        migrations.AddField(
            model_name='services',
            name='app_download_text',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='buttom_image1',
            field=models.ImageField(null=True, upload_to=b'services/buttom', blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='buttom_image2',
            field=models.ImageField(null=True, upload_to=b'services/buttom', blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='buttom_image3',
            field=models.ImageField(null=True, upload_to=b'services/buttom', blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='comment',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='comment_by',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='comment_text',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='promo_description1',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='promo_description2',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='promo_description3',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='promo_image1',
            field=models.ImageField(null=True, upload_to=b'services/promo', blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='promo_image2',
            field=models.ImageField(null=True, upload_to=b'services/promo', blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='promo_image3',
            field=models.ImageField(null=True, upload_to=b'services/promo', blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='reword_title',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='desc2',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
