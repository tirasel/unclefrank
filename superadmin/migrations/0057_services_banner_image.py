# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0056_auto_20170413_1355'),
    ]

    operations = [
        migrations.AddField(
            model_name='services',
            name='banner_image',
            field=models.ImageField(null=True, upload_to=b'services/banner', blank=True),
        ),
    ]
