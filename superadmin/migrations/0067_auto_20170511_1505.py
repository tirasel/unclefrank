# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0066_pushmessage'),
    ]

    operations = [
        migrations.AddField(
            model_name='pushmessage',
            name='courier_job_cancel_by_system',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='stringsetup',
            name='credit_terms_error',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
