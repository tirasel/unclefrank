# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0012_auto_20160809_0946'),
    ]

    operations = [
        migrations.AddField(
            model_name='homecms',
            name='category',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
