# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0054_auto_20170412_1857'),
    ]

    operations = [
        migrations.RenameField(
            model_name='aboutpage',
            old_name='image_title',
            new_name='app_download_link',
        ),
        migrations.RenameField(
            model_name='aboutpage',
            old_name='link1_text',
            new_name='app_download_text',
        ),
        migrations.RenameField(
            model_name='aboutpage',
            old_name='title2',
            new_name='comment',
        ),
        migrations.RenameField(
            model_name='aboutpage',
            old_name='title3',
            new_name='comment_by',
        ),
        migrations.RenameField(
            model_name='aboutpage',
            old_name='desc2',
            new_name='comment_text',
        ),
        migrations.RemoveField(
            model_name='aboutpage',
            name='banner',
        ),
        migrations.RemoveField(
            model_name='aboutpage',
            name='desc3',
        ),
        migrations.RemoveField(
            model_name='aboutpage',
            name='image4',
        ),
        migrations.RemoveField(
            model_name='aboutpage',
            name='video',
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='link',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='link_desc',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='link_text',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team1_desination',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team1_image',
            field=models.ImageField(null=True, upload_to=b'about', blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team1_name',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team2_desination',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team2_image',
            field=models.ImageField(null=True, upload_to=b'about', blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team2_name',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team3_desination',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team3_image',
            field=models.ImageField(null=True, upload_to=b'about', blank=True),
        ),
        migrations.AddField(
            model_name='aboutpage',
            name='team3_name',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='image2',
            field=models.ImageField(null=True, upload_to=b'about', blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='image3',
            field=models.ImageField(null=True, upload_to=b'about', blank=True),
        ),
    ]
