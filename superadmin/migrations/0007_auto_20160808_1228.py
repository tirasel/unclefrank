# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0006_auto_20160808_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutpage',
            name='banner',
            field=models.ImageField(null=True, upload_to=b'about/banner', blank=True),
        ),
    ]
