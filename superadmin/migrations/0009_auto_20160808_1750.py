# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0008_joinus_services'),
    ]

    operations = [
        migrations.AlterField(
            model_name='services',
            name='rewards1_image',
            field=models.ImageField(null=True, upload_to=b'services/reward', blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='rewards2_image',
            field=models.ImageField(null=True, upload_to=b'services/reward', blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='rewards3_image',
            field=models.ImageField(null=True, upload_to=b'services/reward', blank=True),
        ),
        migrations.AlterField(
            model_name='services',
            name='rewards4_image',
            field=models.ImageField(null=True, upload_to=b'services/reward', blank=True),
        ),
    ]
