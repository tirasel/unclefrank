# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0068_auto_20170511_1838'),
    ]

    operations = [
        migrations.AddField(
            model_name='services',
            name='buttom_title',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='description_for_nextday',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='description_for_same_day',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='title_for_same_day',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='services',
            name='title_for_same_nextday',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
