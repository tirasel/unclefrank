# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0005_auto_20160808_1211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homecms',
            name='feature1_image',
            field=models.ImageField(null=True, upload_to=b'home/feature', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature2_image',
            field=models.ImageField(null=True, upload_to=b'home/feature', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature3_image',
            field=models.ImageField(null=True, upload_to=b'home/feature', blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature4_image',
            field=models.ImageField(null=True, upload_to=b'home/feature', blank=True),
        ),
    ]
