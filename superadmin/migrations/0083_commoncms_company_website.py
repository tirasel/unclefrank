# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0082_auto_20170711_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='commoncms',
            name='company_website',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
