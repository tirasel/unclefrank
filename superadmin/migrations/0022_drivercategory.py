# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0021_auto_20161108_1627'),
    ]

    operations = [
        migrations.CreateModel(
            name='DriverCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_name', models.CharField(max_length=120, null=True)),
                ('category_details', models.CharField(max_length=120, null=True)),
            ],
        ),
    ]
