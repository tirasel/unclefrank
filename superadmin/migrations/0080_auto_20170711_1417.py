# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0079_pushmessage_charged_for_ride_job'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pushmessage',
            old_name='charged_for_ride_job',
            new_name='carged_for_ride_job',
        ),
    ]
