# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0073_stringsetup_disclaimer_on_package_guide'),
    ]

    operations = [
        migrations.AlterField(
            model_name='joinus',
            name='feature_title1',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='joinus',
            name='feature_title2',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='joinus',
            name='feature_title3',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='joinus',
            name='feature_title4',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='joinus',
            name='feature_title5',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='joinus',
            name='title1',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='joinus',
            name='title2',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='joinus',
            name='title3',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
