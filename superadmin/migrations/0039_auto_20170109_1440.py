# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0038_auto_20170109_1424'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='drivercategory',
            name='max_number_of_active_job',
        ),
        migrations.AddField(
            model_name='deliverytiming',
            name='max_number_of_active_job',
            field=models.IntegerField(default=1000),
        ),
    ]
