# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0062_auto_20170427_1347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commoncms',
            name='company_address_latitude',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commoncms',
            name='company_address_longitude',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
