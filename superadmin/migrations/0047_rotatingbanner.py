# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0046_auto_20170124_1822'),
    ]

    operations = [
        migrations.CreateModel(
            name='RotatingBanner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(null=True, upload_to=b'home/banner', blank=True)),
                ('title', models.CharField(max_length=120, null=True, blank=True)),
            ],
        ),
    ]
