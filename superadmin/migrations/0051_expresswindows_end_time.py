# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0050_remove_expresswindows_end_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='expresswindows',
            name='end_time',
            field=models.TimeField(null=True, blank=True),
        ),
    ]
