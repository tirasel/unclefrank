# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0080_auto_20170711_1417'),
    ]

    operations = [
        migrations.AddField(
            model_name='pushmessage',
            name='carged_for_ride_cancellation',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
