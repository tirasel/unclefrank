# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0071_auto_20170515_1638'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutpage',
            name='comment',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='aboutpage',
            name='comment_by',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
