# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0052_auto_20170412_1830'),
    ]

    operations = [
        migrations.RenameField(
            model_name='homecms',
            old_name='patner_image_6',
            new_name='patner_image_4',
        ),
    ]
