# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0020_deliverytiming'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliverytiming',
            name='window_name',
            field=models.CharField(max_length=120, null=True),
        ),
        migrations.AlterField(
            model_name='deliverytiming',
            name='end_time',
            field=models.TimeField(null=True),
        ),
        migrations.AlterField(
            model_name='deliverytiming',
            name='start_time',
            field=models.TimeField(null=True),
        ),
    ]
