# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0053_auto_20170412_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homecms',
            name='features_desc',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
