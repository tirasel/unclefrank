# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0085_pushmessage_job_assigned_for_driver'),
    ]

    operations = [
        migrations.AddField(
            model_name='pushmessage',
            name='driver_arrived',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
