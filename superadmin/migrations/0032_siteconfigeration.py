# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0031_auto_20161128_1840'),
    ]

    operations = [
        migrations.CreateModel(
            name='SiteConfigeration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('max_delivery_ojb', models.IntegerField()),
            ],
        ),
    ]
