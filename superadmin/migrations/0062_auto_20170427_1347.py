# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0061_stringsetup'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommonCms',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('logo', models.ImageField(null=True, upload_to=b'common', blank=True)),
                ('logo_white_color', models.ImageField(null=True, upload_to=b'common', blank=True)),
                ('under_banner_title', models.CharField(max_length=120, null=True, blank=True)),
                ('facebook_image', models.ImageField(null=True, upload_to=b'common', blank=True)),
                ('facebook_link', models.CharField(max_length=255, null=True, blank=True)),
                ('twitter_image', models.ImageField(null=True, upload_to=b'common', blank=True)),
                ('twitter_link', models.CharField(max_length=255, null=True, blank=True)),
                ('instagram_image', models.ImageField(null=True, upload_to=b'common', blank=True)),
                ('instagram_link', models.CharField(max_length=255, null=True, blank=True)),
                ('youtube_image', models.ImageField(null=True, upload_to=b'common', blank=True)),
                ('youtube_link', models.CharField(max_length=255, null=True, blank=True)),
                ('android_app_download_link', models.CharField(max_length=255, null=True, blank=True)),
                ('iso_app_download_link', models.CharField(max_length=255, null=True, blank=True)),
                ('copyright_text', models.CharField(max_length=255, null=True, blank=True)),
                ('company_address', models.TextField(null=True, blank=True)),
                ('company_address_latitude', models.TextField(null=True, blank=True)),
                ('company_address_longitude', models.TextField(null=True, blank=True)),
                ('company_phone_no', models.CharField(max_length=255, null=True, blank=True)),
                ('company_fax_no', models.CharField(max_length=255, null=True, blank=True)),
                ('company_email', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='logo',
        ),
        migrations.AlterField(
            model_name='homecms',
            name='banner_desc',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='banner_text',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature1_title',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature2_title',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature3_title',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='feature4_title',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='features',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='features_desc',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='service1_title',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='service2_title',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='under_banner_text',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homecms',
            name='under_banner_title',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
