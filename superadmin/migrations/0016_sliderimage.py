# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0015_remove_homecms_app_text'),
    ]

    operations = [
        migrations.CreateModel(
            name='SliderImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_title', models.CharField(max_length=120, null=True, blank=True)),
                ('image_description', models.TextField(null=True, blank=True)),
                ('the_images', models.ImageField(null=True, upload_to=b'about/iteam', blank=True)),
            ],
        ),
    ]
