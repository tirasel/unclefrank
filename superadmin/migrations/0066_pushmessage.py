# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0065_siteconfigeration_cancellatin_charge'),
    ]

    operations = [
        migrations.CreateModel(
            name='PushMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('new_courier_job', models.CharField(max_length=500, null=True, blank=True)),
                ('courier_job_cancel_by_driver', models.CharField(max_length=500, null=True, blank=True)),
                ('courier_job_cancel_by_customer', models.CharField(max_length=500, null=True, blank=True)),
                ('confirm_customer_ride_taken', models.CharField(max_length=500, null=True, blank=True)),
                ('confirm_customer_ride_completed', models.CharField(max_length=500, null=True, blank=True)),
                ('ride_cancel_by_driver', models.CharField(max_length=500, null=True, blank=True)),
                ('ride_cancel_by_customer', models.CharField(max_length=500, null=True, blank=True)),
            ],
        ),
    ]
