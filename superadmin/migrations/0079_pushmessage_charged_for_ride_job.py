# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0078_joinus_linke_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='pushmessage',
            name='charged_for_ride_job',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
