# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0028_aboutpage_image_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='joinus',
            name='desc4',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='footer_slider_image1',
            field=models.ImageField(null=True, upload_to=b'services/slider', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='footer_slider_image2',
            field=models.ImageField(null=True, upload_to=b'services/slider', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='footer_slider_image3',
            field=models.ImageField(null=True, upload_to=b'services/slider', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='footer_slider_image4',
            field=models.ImageField(null=True, upload_to=b'services/slider', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='footer_slider_image5',
            field=models.ImageField(null=True, upload_to=b'services/slider', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='title4',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
