# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0059_auto_20170413_1656'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactuspage',
            name='addr_email',
        ),
        migrations.RemoveField(
            model_name='contactuspage',
            name='addr_fax',
        ),
        migrations.RemoveField(
            model_name='contactuspage',
            name='addr_tel',
        ),
        migrations.RemoveField(
            model_name='contactuspage',
            name='addr_title',
        ),
        migrations.RemoveField(
            model_name='contactuspage',
            name='contact_location',
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='bottom_description',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_desc1',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_desc2',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_desc3',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_image1',
            field=models.ImageField(null=True, upload_to=b'contactus/location', blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_image2',
            field=models.ImageField(null=True, upload_to=b'contactus/location', blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_image3',
            field=models.ImageField(null=True, upload_to=b'contactus/location', blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_title1',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_title2',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='contactuspage',
            name='location_title3',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
