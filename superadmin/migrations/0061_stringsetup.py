# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0060_auto_20170413_1731'),
    ]

    operations = [
        migrations.CreateModel(
            name='StringSetup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('registration_error_text', models.CharField(max_length=255, null=True)),
                ('login_error_text', models.CharField(max_length=255, null=True)),
                ('no_driver_found_text', models.CharField(max_length=255, null=True)),
                ('pickup_job_taken_text', models.CharField(max_length=255, null=True)),
                ('pickup_job_cancelled_text', models.CharField(max_length=255, null=True)),
                ('new_courier_job_post', models.CharField(max_length=255, null=True)),
            ],
        ),
    ]
