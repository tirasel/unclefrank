# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0083_commoncms_company_website'),
    ]

    operations = [
        migrations.AddField(
            model_name='commoncms',
            name='dashboard_payment_page_header_image',
            field=models.ImageField(null=True, upload_to=b'common', blank=True),
        ),
        migrations.AddField(
            model_name='commoncms',
            name='dashboard_payment_page_header_text',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
