# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('superadmin', '0037_auto_20170103_1849'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessControl',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_cms', models.BooleanField(default=False)),
                ('is_settings', models.BooleanField(default=False)),
                ('is_courier', models.BooleanField(default=False)),
                ('is_pickup', models.BooleanField(default=False)),
                ('is_driver_info', models.BooleanField(default=False)),
                ('is_sales_report', models.BooleanField(default=False)),
                ('is_customer_list', models.BooleanField(default=False)),
                ('is_account', models.BooleanField(default=False)),
                ('is_add_coupon', models.BooleanField(default=False)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
