# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0074_auto_20170618_1517'),
    ]

    operations = [
        migrations.AlterField(
            model_name='joinus',
            name='linke',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
