# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0029_auto_20161121_1748'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommissionSetup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('commission_pickup', models.DecimalField(max_digits=5, decimal_places=2)),
                ('commission_courier', models.DecimalField(max_digits=5, decimal_places=2)),
                ('category', models.ForeignKey(to='superadmin.DriverCategory')),
            ],
        ),
    ]
