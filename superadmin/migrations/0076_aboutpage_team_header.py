# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0075_auto_20170618_1526'),
    ]

    operations = [
        migrations.AddField(
            model_name='aboutpage',
            name='team_header',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
