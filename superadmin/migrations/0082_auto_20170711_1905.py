# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0081_pushmessage_carged_for_ride_cancellation'),
    ]

    operations = [
        migrations.AddField(
            model_name='pushmessage',
            name='other_device_login_for_consumer',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='pushmessage',
            name='other_device_login_for_driver',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
