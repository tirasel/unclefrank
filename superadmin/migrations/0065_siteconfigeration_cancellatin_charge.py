# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0064_auto_20170427_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfigeration',
            name='cancellatin_charge',
            field=models.DecimalField(default=6.0, max_digits=7, decimal_places=2),
        ),
    ]
