# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0044_auto_20170122_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='expresswindows',
            name='end_time',
            field=models.TimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='expresswindows',
            name='start_time',
            field=models.TimeField(null=True, blank=True),
        ),
    ]
