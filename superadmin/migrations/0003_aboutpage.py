# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0002_auto_20160807_0546'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('banner', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('title1', models.CharField(max_length=120, null=True, blank=True)),
                ('desc1', models.CharField(max_length=500, null=True, blank=True)),
                ('image1', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('title2', models.CharField(max_length=120, null=True, blank=True)),
                ('desc2', models.CharField(max_length=500, null=True, blank=True)),
                ('link1_text', models.CharField(max_length=120, null=True, blank=True)),
                ('title3', models.CharField(max_length=120, null=True, blank=True)),
                ('desc3', models.CharField(max_length=500, null=True, blank=True)),
                ('video', models.FileField(null=True, upload_to=b'', blank=True)),
                ('image2', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('image3', models.ImageField(null=True, upload_to=b'', blank=True)),
                ('image4', models.ImageField(null=True, upload_to=b'', blank=True)),
            ],
        ),
    ]
