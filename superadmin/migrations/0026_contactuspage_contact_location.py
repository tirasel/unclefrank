# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0025_expresswindows'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactuspage',
            name='contact_location',
            field=models.CharField(max_length=120, null=True),
        ),
    ]
