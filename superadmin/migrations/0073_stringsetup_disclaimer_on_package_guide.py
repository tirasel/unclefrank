# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0072_auto_20170516_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='stringsetup',
            name='disclaimer_on_package_guide',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
