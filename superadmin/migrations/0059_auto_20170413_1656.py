# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0058_auto_20170413_1625'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='joinus',
            name='desc1',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='desc2',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='desc4',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature1_desc',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature1_image',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature1_title',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature2_desc',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature2_image',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature2_title',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature3_desc',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature3_image',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature3_title',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature4_desc',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature4_image',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='feature4_title',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='features_desc',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='features_text',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='footer_slider_image1',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='footer_slider_image2',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='footer_slider_image3',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='footer_slider_image4',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='footer_slider_image5',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='image1',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='image2',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='image3',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='link1_text',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='slider_image1',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='slider_image2',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='slider_image3',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='slider_image4',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='slider_image5',
        ),
        migrations.RemoveField(
            model_name='joinus',
            name='title4',
        ),
        migrations.AddField(
            model_name='joinus',
            name='banner1',
            field=models.ImageField(null=True, upload_to=b'joinus/banner', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='banner2',
            field=models.ImageField(null=True, upload_to=b'joinus/banner', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_desc1',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_desc2',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_desc3',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_desc4',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_desc5',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_image1',
            field=models.ImageField(null=True, upload_to=b'joinus/feature', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_image2',
            field=models.ImageField(null=True, upload_to=b'joinus/feature', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_image3',
            field=models.ImageField(null=True, upload_to=b'joinus/feature', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_image4',
            field=models.ImageField(null=True, upload_to=b'joinus/feature', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_image5',
            field=models.ImageField(null=True, upload_to=b'joinus/feature', blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_title1',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_title2',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_title3',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_title4',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='feature_title5',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='joinus',
            name='linke',
            field=models.TextField(null=True, blank=True),
        ),
    ]
