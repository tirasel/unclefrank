# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0051_expresswindows_end_time'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='homecms',
            name='category',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer1_desc',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer1_text',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer2_text',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer2_url1',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer2_url2',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer2_url3',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer2_url4',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer2_url5',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer3_url1',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer3_url2',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer3_url3',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='footer4',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='social1_icons',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='social2_icons',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='social3_icons',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='social4_icons',
        ),
        migrations.RemoveField(
            model_name='homecms',
            name='social5_icons',
        ),
        migrations.AddField(
            model_name='homecms',
            name='patner_image_1',
            field=models.ImageField(null=True, upload_to=b'home/patner', blank=True),
        ),
        migrations.AddField(
            model_name='homecms',
            name='patner_image_2',
            field=models.ImageField(null=True, upload_to=b'home/patner', blank=True),
        ),
        migrations.AddField(
            model_name='homecms',
            name='patner_image_3',
            field=models.ImageField(null=True, upload_to=b'home/patner', blank=True),
        ),
        migrations.AddField(
            model_name='homecms',
            name='patner_image_5',
            field=models.ImageField(null=True, upload_to=b'home/patner', blank=True),
        ),
        migrations.AddField(
            model_name='homecms',
            name='patner_image_6',
            field=models.ImageField(null=True, upload_to=b'home/patner', blank=True),
        ),
        migrations.AddField(
            model_name='homecms',
            name='under_banner_title',
            field=models.CharField(max_length=120, null=True, blank=True),
        ),
    ]
