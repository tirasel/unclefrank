from django.conf.urls import url, include
from rest_framework import routers
from apis import views
from rest_framework.authtoken import views as restviews

router = routers.DefaultRouter()

# router.register(r'driver', views.UserViewSet)
router.register(r'delevery', views.DeleveryServiceViewSet)
router.register(r'vechile_type', views.VechileTypeViewSet)
router.register(r'vehicle_type_new', views.VechileTypeViewSetRide)
# router.register(r'vechile_type_courier', views.VechileTypeViewSetCourier)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^customer/completeRegistration/(?P<customer_id>\d+)/$', views.complete_registration),
    url(r'^pickup/(?P<pickup_id>\d+)/$', views.get_pickup_detail),
    url(r'^customer/couriers/$', views.get_customer_couriers),
    ############New Customer APIS###################
    url(r'^customer/active_bookings/(?P<customer_id>\d+)/$', views.active_bookings),
    url(r'^customer/active_bookings_alternative/(?P<customer_id>\d+)/$', views.active_bookings_alternative),
    url(r'^courier/(?P<delivery_id>\d+)/$', views.get_delivery_detail),
    url(r'^customer/rate_driver/$', views.rate_driver),
    url(r'^customer/pickups/$', views.get_customer_pickups),
    url(r'^customer/(?P<customer_id>\d+)/$', views.get_customer_info),
    url(r'^customer/courier_booking/$', views.courier_booking),
    ############End New Customer APIS##############
    url(r'^authentication/$', views.custom_authentication), #checked
    url(r'^api-token-auth/', restviews.obtain_auth_token),
    # url(r'^driver/dashboard/', views.dashboard),
    url(r'^driver/dashboard/', views.dashboard),
    url(r'^take_delevery_job/(?P<job_id>\d+)/$', views.take_delevery_service),
    # url(r'^pickedup_delevery_job/(?P<job_id>\d+)/$', views.pickedup_delevery_service),
    url(r'^verification/(?P<verification_id>\d+)/$', views.verification),
    url(r'^customer_verification/(?P<verification_id>\d+)/$', views.customer_verification),
    url(r'^verification_check/(?P<user_id>\d+)/$', views.verification_check),
    url(r'^take_pickup_job/(?P<job_id>\d+)/$', views.take_pickup_service),
    url(r'^pickedup_pickup_job/(?P<job_id>\d+)/$', views.pickedup_pickup_service),
    url(r'^complete_delevery_job/(?P<job_id>\d+)/$', views.complete_delevery_job),
    url(r'^cancell_delevery_job_by_customer_new/$', views.cancell_delevery_job_by_customer_new),
    url(r'^cancell_delevery_job/(?P<job_id>\d+)/$', views.cancell_delevery_job),
    url(r'^cancell_delevery_job_by_customer/(?P<job_id>\d+)/$', views.cancell_delevery_job_by_customer),
    url(r'^get_fare_details/(?P<job_id>\d+)/$', views.get_fare_details),
    url(r'^complete_pickup_job/(?P<job_id>\d+)/$', views.complete_pickup_job),
    url(r'^cancell_pickup_job/(?P<job_id>\d+)/$', views.cancell_pickup_job),
    url(r'^confirm_pickup/$', views.confirm_pickup),
    url(r'^get_pickup_price/(?P<type_id>\d+)/$', views.get_pickup_price),
    url(r'^get_driver_location/(?P<driver_id>\d+)/$', views.get_driver_location_by_id),
    url(r'^update_account_info/$', views.update_your_account),
    url(r'^driver/$', views.create_driver),
    url(r'^update_drivers_image/$', views.update_drivers_image),
    url(r'^logout/$', views.logout),
    url(r'^driver-location/$', views.update_driver_location),
    url(r'^customer_location/$', views.update_customer_location),
    # url(r'^get_activity_log_delevery/(?P<job_id>\d+)/$', 'apis.views.get_activity_log')
    url(r'^customer/verify/$', views.verify_code),
    url(r'^customer/$', views.create_customer),
    # url(r'^pending_delivery_job/(?P<driver_id>\d+)/$', views.get_pending_delivery_job), #checked
    # url(r'^taken_delivery_job/(?P<driver_id>\d+)/$', views.get_taken_delivery_job), #checked
    # url(r'^completed_delivery_job/(?P<driver_id>\d+)/$', views.get_completed_delivery_job), #checked
    # url(r'^pending_pickup_job/(?P<driver_id>\d+)/$', views.get_pending_pickup_job), #checked
    # url(r'^taken_pickup_job/(?P<driver_id>\d+)/$', views.get_taken_pickup_job), #checked
    # url(r'^completed_pickup_job/(?P<driver_id>\d+)/$', views.get_completed_pickup_job), #checked
    # url(r'^both_jobs/(?P<driver_id>\d+)/$', views.get_both_pending_job), #checked
    # url(r'^both_taken_jobs/(?P<driver_id>\d+)/$', views.get_both_taken_job),   #checked
    # url(r'^both_completed_jobs/(?P<driver_id>\d+)/$', views.get_both_completed_job), #checked
    url(r'^get_driver_location/$', views.get_driver_location),
    url(r'^get_customer_location/$', views.get_customer_location),
    url(r'^get_pickup_status/(?P<pickup_id>\d+)/$', views.get_pickup_status),
    url(r'^get_address/(?P<postal_code>\d+)/$', views.get_address),
    url(r'^get_distance_time/(?P<pickup_id>\d+)/$', views.get_updated_distance),
    url(r'^location_tracker/(?P<job_id>\d+)/$', views.location_tracker),
    url(r'^get_listed_jobs/$', views.get_listed_jobs),
    url(r'^get_refined_jobs_list/$', views.refined_job_list),
    url(r'^available_toggle/$', views.available_toggle),
    url(r'^get_posted_job_by_date/$', views.posted_job_by_date),
    url(r'^get_pickup_job_details/$', views.get_pickup_job_details),
    url(r'^update_account_settings/$', views.update_account_settings),
    url(r'^change_password_for_driver/$', views.change_password_for_driver),
    url(r'^get_courier_price_details/$', views.get_courier_price_details),
    url(r'^get_drivers_details/$', views.get_drivers_details),
    # Log if the josb view by driver
    url(r'^pickup_job_view/$', views.pickup_job_view),
    url(r'^test_email/$', views.test_email),
    url(r'^test_gmap/$', views.test_gmap),
    url(r'^reset_password/$', views.reset_password),
    url(r'^get_deliver_window/$', views.get_deliver_window),
    url(r'^get_collection_window/$', views.get_collection_window),
    url(r'^get_pricing/$', views.get_pricing),
    url(r'^get_credit_card_information/$', views.get_credit_card_information),
    url(r'^make_charge/$', views.make_charge),
    url(r'^add_card/$', views.add_card),
    url(r'^make_payment_with_card/$', views.make_payment_with_card),
    url(r'^add_card/$', views.add_card),
    url(r'^remove_card/$', views.remove_card),
    url(r'^edit_card/$', views.edit_card),
    #get nearest drivers location
    url(r'^get_nearest_drivers_location/$', views.get_nearest_drivers_location),
    url(r'^requer_for_pickup/$', views.requer_for_pickup),
    url(r'^get_driver/(?P<driver_id>\d+)/$', views.get_driver),
    url(r'^get_cancel_reason/$', views.get_cancel_reason),
    url(r'^get_courier_cancel_reason/$', views.get_courier_cancel_reason),
    url(r'^cancell_pickup_by_customer/$', views.cancell_pickup_by_customer),
    url(r'^get_faq/$', views.get_faq),
    url(r'^notification_test/$', views.notification_test),
    url(r'^customer_register_as_driver/$', views.customer_register_as_driver),
    url(r'^driver_register_as_customer/$', views.driver_register_as_customer),
    url(r'^customer_login/$', views.customer_login),
    url(r'^driver_login/$', views.driver_login),
    url(r'^resend_sms/(?P<user_type>[\w\-]+)/(?P<id>\d+)/$', views.resend_sms),
    url(r'^feedback/$', views.feedback),
    url(r'^about/$', views.about),
    url(r'^facebook_signup/$', views.facebook_signup),
    url(r'^facebook_signin/$', views.facebook_signin),
    url(r'^driver_arrival/(?P<job_id>\d+)/$', views.arrival),
    url(r'^get_strings/$', views.get_strings),
    url(r'^get_extimate_price/$', views.get_extimate_price),
    url(r'^get_drivers_eta/$', views.get_drivers_eta),
    url(r'^get_postal_code_sercharge/$', views.get_postal_code_sercharge),
    url(r'^get_credite_terms/$', views.get_credite_terms),
    url(r'^get_dtiver_waiting_time/$', views.get_dtiver_waiting_time),
    url(r'^get_drivers_message/$', views.get_drivers_message),
    url(r'^get_customer_message/$', views.get_customer_message),
    url(r'^get_customer_current_ride_job/$', views.get_customer_current_ride_job),
    url(r'^get_driver_current_ride_job/$', views.get_driver_current_ride_job),
    url(r'^get_customer_details/$', views.get_customer_details),
    url(r'^get_about_us_page/$', views.get_about_us_page),
    url(r'^get_express_price/$', views.get_express_price),
    url(r'^ride_ready_for_driver/$', views.ride_ready_for_driver),
    
]