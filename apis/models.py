from django.db import models
from django.core import serializers
from django.db.models.signals import pre_save
from django.dispatch import receiver
from gadjo.requestprovider.signals import get_request
from service.models import PickUpRate
from base.models import AuditTrile


#Here the The Tuse Audite trile data will be saved by using signals

#
@receiver(models.signals.pre_save)
def _user_audite(sender, instance, **kwargs):
	if sender == PickUpRate :
		trac = AuditTrile()
		http_request = get_request()
		user = http_request.user
		trac.table = instance._meta.db_table
		trac.user_name = user.username
		objname = instance.__class__
		if instance.pk is not None:
			old = objname.objects.get(pk= instance.pk)
			trac.old_data = serializers.serialize('json', [old])
			trac.new_data = serializers.serialize('json', [instance])
			trac.operation = "Edited"
			trac.save()


@receiver(models.signals.post_save)
def _user_audite_add(sender, instance, created, **kwargs):
	if sender == PickUpRate :
		trac = AuditTrile()
		http_request = get_request()
		user = http_request.user
		trac.table = instance._meta.db_table
		trac.user_name = user.username
		objname = instance.__class__
		trac.controller = instance._meta.db_table
		if created:
			trac.operation = "Added"
			trac.old_data = serializers.serialize('json', [instance])
			trac.save()

@receiver(models.signals.pre_delete)
def _user_audite_delete(sender, instance, **kwargs):
    if sender == PickUpRate :
        trac = AuditTrile()
        http_request = get_request()
        user = http_request.user
        trac.table = instance._meta.db_table
        trac.user_name = user.username
        trac.old_data = serializers.serialize('json', [instance])
        trac.operation = "Deleted"
        trac.save()